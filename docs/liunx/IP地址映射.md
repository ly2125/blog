有时候，比如需要将www.shop.com映射到本地，这个就需要用到IP地址映射。

## deeplin

首先我们进入`hosts`文件

```
vim /etc/hosts
```

然后在文件的末尾添加如下

```
127.0.0.1   www.shop.com
```

保存后，验证。进行`ping  www.shop.com`,如果访问的本地就说明配置成功了。