在平时java学习中，我们本地装了虚拟机，每次启动的时候，IP地址就会发生变化，这个时候我们又要去改项目的配置文件，就很烦躁。这个时候我们就会去固定虚拟机的IP地址。

## centerOS

- 步骤一：打开配置文件

```bash
  vim /etc/sysconfig/network-scripts/ifcfg-ens33 
```

- 步骤二：修改配置文件

```bash
BOOTPROTO="static"
ONBOOT="yes"
```

并在后边加上：

```
IPADDR=192.168.124.26 ##服务器的ip
NETMASK=255.255.255.0##子网掩码
GATEWAY=192.168.124.2##网关
DNS1=8.8.8.8
DNS2=8.8.4.4
```

- 步骤三：重启服务器

​           reboot或者手动重启
