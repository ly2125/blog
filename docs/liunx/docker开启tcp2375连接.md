
### 方法一

首先是怎么配置远程访问的API：

```
sudo vim /etc/default/docker
```

加入下面一行

```
DOCKER_OPTS="-H tcp://0.0.0.0:2375"
```

重启docker即可：

```
sudo systemctl restart docker
```

PS:这是网上给的配置方法，也是这种简单配置让Docker Daemon把服务暴露在tcp的2375端口上，这样就可以在网络上操作Docker了。Docker本身没有身份认证的功能，只要网络上能访问到服务端口，就可以操作Docker。

### 方法二
在/usr/lib/systemd/system/docker.service，配置远程访问。

主要是在[Service]这个部分，加上下面两个参数

```
# vim /usr/lib/systemd/system/docker.service

[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix://var/run/docker.sock
```

重启

```
#重新加载配置
systemctl daemon-reload
#重启docker
systemctl restart docker
```
### 方法三
下面修改daemon.json的配置

```
vim /etc/docker/daemon.json
```




```
{
"hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"]
}
```

"unix:///var/run/docker.sock"：unix socket，本地客户端将通过这个来连接 Docker Daemon。
"tcp://0.0.0.0:2375"：tcp socket，表示允许任何远程客户端通过 2375 端口连接 Docker Daemon。

修改配置以后

然后让docker重新读取配置文件,并重启docker服务

```
systemctl daemon-reload
systemctl restart docker
```

查看docker进程：

```
[root@slaver2 ~]# ps -ef|grep docker
root      44221      1  1 18:16 ?        00:00:06 /usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix://var/run/docker.sock
```

Docker守护进程打开一个HTTP Socket,这样才能实现远程通信

### 说明

一般公网IP开启这个要注意，防止他们恶意操作

来源：https://www.cnblogs.com/hongdada/p/11512901.html