﻿
## 系统服务管理

### systemctl

> `systemctl`命令是`service`和`chkconfig`命令的组合体，可用于管理系统。

- 输出系统中各个服务的状态：

```bash
systemctl list-units --type=service
```
- 查看服务的运行状态：

```bash
systemctl status firewalld
```
- 关闭服务：

```bash
systemctl stop firewalld
```
- 启动服务：

```bash
systemctl start firewalld
```


- 重新启动服务（不管当前服务是启动还是关闭）：

```bash
systemctl restart firewalld
```

- 重新载入配置信息而不中断服务：

```bash
systemctl reload firewalld
```

- 禁止服务开机自启动：

```bash
systemctl disable firewalld
```
- 设置服务开机自启动：

```bash
systemctl enable firewalld
```
## 文件管理

### ls

列出指定目录下的所有文件，列出`/`目录下的文件：

```bash
ls -l /
```

> -a 显示所有文件及目录 (. 开头的隐藏文件也会列出)
> -l 除文件名称外，亦将文件型态、权限、拥有者、文件大小等资讯详细列出
> -r 将文件以相反次序显示(原定依英文字母次序)
> -t 将文件依建立时间之先后次序列出
> -A 同 -a ，但不列出 "." (目前目录) 及 ".." (父目录)
> -F 在列出的文件名称后加一符号；例如可执行档则加 "*", 目录则加 "/"
> -R 若目录下有文件，则以下之文件亦皆依序列出

### pwd

获取目前所在工作目录的绝对路径：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210110200945271.png)

### cd

改变当前工作目录：

```bash
cd /usr/local
```

### date

显示或修改系统时间与日期；

```bash
date '+%Y-%m-%d %H:%M:%S'
```


### passwd

用于设置用户密码：

```bash
passwd root
```

### su

改变用户身份（切换到超级用户）：

```bash
su -
```

### clear

用于清除屏幕信息

### man

显示指定命令的帮助信息：

```bash
man ls
```

### who

- 查询系统处于什么运行级别：

```bash
who -r
```


- 显示目前登录到系统的用户：
```bash
who -buT
```

### free

显示系统内存状态（单位MB）：

```bash
free -m
```

### ps

- 显示系统进程运行动态：

```bash
ps -ef
```

- 查看`sshd`进程的运行动态：

```bash
ps -ef | grep sshd
```

> ps aux 是用baiBSD的格式来显示 java这个du进程 显示的项zhi目有：daoUSER , PID , %CPU , %MEM
> , VSZ , RSS , TTY , STAT , START , TIME , COMMAND ps -ef
> 是用标准的zhuan格式显示java这个进shu程 显示的项目有：UID , PID , PPID , C , STIME , TTY ,
> TIME , CMD

### top

查看即时活跃的进程，类似Windows的任务管理器。


### mkdir

创建目录

### more

用于分页查看文件，例如每页10行查看`boot.log`文件：

```bash
more -c -10 /var/log/boot.log
```

### cat

用于查看文件，例如查看Linux启动日志文件文件，并标明行号：

```bash
cat -Ab /var/log/boot.log
```


### touch

用于创建文件，例如创建`text.txt`文件：

```bash
touch text.txt
```

### rm

- 删除文件：

```bash
rm text.txt
```

- 强制删除某个目录及其子目录：

```bash
rm -rf testdir/
```

### cp

用于拷贝文件，例如将`test1`目录复制到`test2`目录

```bash
cp -r /mydata/tes1 /mydata/test2
```

### mv

用于移动或覆盖文件：

```bash
mv text.txt text2.txt
```

## 压缩与解压

### tar

- 将`/etc`文件夹中的文件归档到文件`etc.tar`（并不会进行压缩）：

```bash
tar -cvf /mydata/etc.tar /etc
```

- 用`gzip`压缩文件夹`/etc`中的文件到文件`etc.tar.gz`：

```bash
tar -zcvf /mydata/etc.tar.gz /etc
```

- 用`bzip2`压缩文件夹`/etc`到文件`/etc.tar.bz2`：
## 压缩与解压

### tar



- 解压文件到指定目录（gzip）：

```bash
tar -zxvf /mydata/etc.tar.gz -C /mydata/etc
```

## 磁盘和网络管理

### df

查看磁盘空间占用情况：

```bash
df -hT
```

![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-3FvMiUW7-1610280071987)(../images/linux_command_25.png)\]](https://img-blog.csdnimg.cn/20210110203921933.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMyNzQ4ODY5,size_16,color_FFFFFF,t_70)


### dh

查看当前目录下的文件及文件夹所占大小：

```bash
du -h --max-depth=1 ./*
```

[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-qZ7wX5yK-1610280071988)(../images/linux_command_26.png)]

### ifconfig

显示当前网络接口状态：

[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-BxdM7Ttz-1610280071989)(../images/linux_command_27.png)]

### netstat

- 查看当前路由信息：

```bash
netstat -rn
```

[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-JocndNWg-1610280071990)(../images/linux_command_28.png)]

- 查看所有有效TCP连接：

```bash
netstat -an
```

- 查看系统中启动的监听服务：

```bash
netstat -tulnp
```

[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-emxcC2Dp-1610280071994)(../images/linux_command_29.png)]

- 查看处于连接状态的系统资源信息：

```bash
netstat -atunp
```

### wget

从网络上下载文件

[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-j2MhTkAu-1610280071995)(../images/linux_command_30.png)]

## 文件上传下载

- 安装上传下载工具`lrzsz`；

```bash
yum install -y lrzsz
```

- 上传文件，输入以下命令`XShell`会弹出文件上传框；

```bash
rz
```

- 下载文件，输入以下命令`XShell`会弹出文件保存框；

```bash
sz fileName
```

## 软件的安装与管理

### rpm

> RPM是`Red-Hat Package Manager`的缩写，一种Linux下通用的软件包管理方式，可用于安装和管理`.rpm`结尾的软件包。

- 安装软件包：

```bash
rpm -ivh nginx-1.12.2-2.el7.x86_64.rpm
```

- 模糊搜索软件包：

```bash
rpm -qa | grep nginx
```

- 精确查找软件包：

```bash
rpm -qa nginx
```

- 查询软件包的安装路径：

```bash
rpm -ql nginx-1.12.2-2.el7.x86_64
```

- 查看软件包的概要信息：

```bash
rpm -qi nginx-1.12.2-2.el7.x86_64
```
- 验证软件包内容和安装文件是否一致：

```bash
rpm -V nginx-1.12.2-2.el7.x86_64
```

- 更新软件包：

```bash
rpm -Uvh nginx-1.12.2-2.el7.x86_64
```

- 删除软件包：

```bash
rpm -e nginx-1.12.2-2.el7.x86_64
```

### yum

> Yum是`Yellow dog Updater, Modified`的缩写，能够在线自动下载RPM包并安装，可以自动处理依赖性关系，并且一次安装所有依赖的软件包，非常方便！

- 安装软件包： 

```bash
yum install nginx
```
- 检查可以更新的软件包：

```bash
yum check-update
```

- 更新指定的软件包：

```bash
yum update nginx
```

- 在资源库中查找软件包信息：

```bash
yum info nginx*
```

- 列出已经安装的所有软件包：

```bash
yum info installed
```
- 列出软件包名称：

```bash
yum list nginx*
```

- 模糊搜索软件包：

```bash
yum search nginx
```

## 公众号

![公众号图片](https://img-blog.csdnimg.cn/img_convert/b6cdca2313590a5f3aaaf676ae35770c.png)
`Yellow dog Updater, Modified`的缩写，能够在线自动下载RPM包并安装，可以自动处理依赖性关系，并且一次安装所有依赖的软件包，非常方便！

- 安装软件包： 

```bash
yum install nginx
```
- 检查可以更新的软件包：

```bash
yum check-update
```

- 更新指定的软件包：

```bash
yum update nginx
```

- 在资源库中查找软件包信息：

```bash
yum info nginx*
```

- 列出已经安装的所有软件包：

```bash
yum info installed
```
- 列出软件包名称：

```bash
yum list nginx*
```

- 模糊搜索软件包：

```bash
yum search nginx
```

## 公众号

[外链图片转存中...(img-RcJlFvfV-1610280071998)]
