## idea 常用插件
  ### translate
  英语不够，translate来凑！
  ### Maven Helper
  帮助我们查看、并解决maven依赖冲突。一旦安装了Maven Helper插件，只要打开pom文件，就可以打开该pom文件的Dependency Analyzer视图（在文件打开之后，文件下面会多出这样一个tab），
进入Dependency Analyzer视图之后有三个查看选项，分别是Conflicts(冲突)、All Dependencies as List(列表形式查看所有依赖)、All Dependencies as Tree(树结构查看所有依赖)。并且这个页面还支持搜索。很方便！并且使用该插件还能快速的执行maven命令。



## 配置RunDashboard

在workspace中.dead文件下找到RunDashboard，再里边添加

```xml
<option name="configurationTypes">
  <set>
    <option value="SpringBootApplicationConfigurationType" />
  </set>
</option>
```

添加后如下：

```xml
<component name="RunDashboard">
 <option name="ruleStates">
     <list>
       <RuleState>
           <option name="name" value="ConfigurationTypeDashboardGroupingRule" />
       </RuleState>
       <RuleState>
          <option name="name" value="StatusDashboardGroupingRule" />
       </RuleState>
     </list>
  </option>
  <option name="contentProportion" value="0.22874807" />
  <option name="configurationTypes">
     <set>
          <option value="SpringBootApplicationConfigurationType" />
     </set>
  </option>
 </component>
```

效果如下：

![image-20210327100755906](https://luyi-docs.oss-cn-chengdu.aliyuncs.com/blog/20210327100806.png)



## 

## IntelliJ IDEA 简体中文专题教程

https://github.com/judasn/IntelliJ-IDEA-Tutorial
