



##   压缩包安装

﻿### 下载

 下载地址：[https://www.redis.net.cn/download/](%E4%B8%8B%E8%BD%BD%E5%9C%B0%E5%9D%80%EF%BC%9Ahttps://www.redis.net.cn/download/%20%20,%E8%BF%99%E9%87%8C%E4%BB%A5redis-5.0.4.tar.gz%E4%B8%BA%E4%BE%8B%EF%BC%8C%E6%88%96%E8%80%85%E4%BD%BF%E7%94%A8wget%E4%B8%8B%E8%BD%BD%EF%BC%8C%E4%B8%8B%E8%BD%BD%E7%9A%84%E5%91%BD%E4%BB%A4%EF%BC%9A%20%20%20wget%20http://download.redis.io/releases/redis-5.0.4.tar.gz)  ,这里以redis-5.0.4.tar.gz为例，或者使用wget下载，下载的命令：
```java
wget http://download.redis.io/releases/redis-5.0.4.tar.gz
```
### 解压
   上传到指定liunx的路径，然后解压，解压的命令：tar -zxvf  redis-5.0.4.tar.gz

### 编译并安装到指定的目录
```java
make  PREFIX=/usr/local/redis-5.0.4 install
```
说明：编译需要确保安装了gcc，没有就用以下命令安装

```java
yum install gcc
```

### 启动

```java
./bin/redis-server ./redis.conf  
```
说明:如果不需要指定配置文件，就不需要./redis.conf

### 编译常见的错误
#### gcc版本太低
如下，解决办法，参考：[https://www.jianshu.com/p/0b4ab97f4c5b](https://www.jianshu.com/p/0b4ab97f4c5b)
```java
erver.c:5117:168: 错误：‘struct redisServer’没有名为‘sentinel_mode’的成员
         serverLog(LL_WARNING, "Warning: no config file specified, using the default config. In order to specify a config file use %s /path/to/%s.conf", argv[0], server.sentinel_mode ? "sentinel" : "redis");
                                                                                                                                                                        ^
server.c:5122:11: 错误：‘struct redisServer’没有名为‘supervised’的成员
     server.supervised = redisIsSupervised(server.supervised_mode);
           ^
server.c:5122:49: 错误：‘struct redisServer’没有名为‘supervised_mode’的成员
     server.supervised = redisIsSupervised(server.supervised_mode);
                                                 ^
server.c:5123:28: 错误：‘struct redisServer’没有名为‘daemonize’的成员
     int background = server.daemonize && !server.supervised;
                            ^
server.c:5123:49: 错误：‘struct redisServer’没有名为‘supervised’的成员
     int background = server.daemonize && !server.supervised;
                                                 ^
server.c:5127:29: 错误：‘struct redisServer’没有名为‘pidfile’的成员
     if (background || server.pidfile) createPidFile();
                             ^
server.c:5132:16: 错误：‘struct redisServer’没有名为‘sentinel_mode’的成员
     if (!server.sentinel_mode) {
                ^
server.c:5142:19: 错误：‘struct redisServer’没有名为‘cluster_enabled’的成员
         if (server.cluster_enabled) {
                   ^
server.c:5150:19: 错误：‘struct redisServer’没有名为‘ipfd_count’的成员
         if (server.ipfd_count > 0 || server.tlsfd_count > 0)
                   ^
server.c:5150:44: 错误：‘struct redisServer’没有名为‘tlsfd_count’的成员
         if (server.ipfd_count > 0 || server.tlsfd_count > 0)
                                            ^
server.c:5152:19: 错误：‘struct redisServer’没有名为‘sofd’的成员
         if (server.sofd > 0)
```

#### redis编译报致命错误：jemalloc/jemalloc.h：没有那个文件或目录 
分配器allocator， 如果有MALLOC  这个 环境变量， 会有用这个环境变量的 去建立Redis。而且libc 并不是默认的 分配器， 默认的是 jemalloc, 因为 jemalloc 被证明 有更少的 fragmentation problems 比libc。但是如果你又没有jemalloc 而只有 libc 当然 make 出错。 所以加这么一个参数,运行如下命令：make MALLOC=libc

这里是redis的readme.md中的说明

```java
Selecting a non-default memory allocator when building Redis is done by setting
the `MALLOC` environment variable. Redis is compiled and linked against libc
malloc by default, with the exception of jemalloc being the default on Linux
systems. This default was picked because jemalloc has proven to have fewer
fragmentation problems than libc malloc.

To force compiling against libc malloc, use:

    % make MALLOC=libc

To compile against jemalloc on Mac OS X systems, use:

    % make MALLOC=jemalloc

Verbose build
```



##  docker 安装

### 镜像拉取 

首先到docker hup拉取需要的对象，这里以最新的版本为例

```
docker pull redis
```

拉取后我们可以用`docker image` 找到对应的镜像

### 准备挂载目录



- 新建conf（配置文件目录）,data（存放数据）两个目录（这里我选择目录是：/usr/local/soft/docker/redis）
- 到redis官网下载对应的配置文件，地址[点我](https://redis.io/topics/config/),并放到conf下



### docker启动



```
docker run --name redis -p 6379:6379 \
-v /usr/local/soft/docker/redis/conf/redis.conf:/etc/redis/redis.conf \
-v /usr/local/soft/docker/redis/data:/data \
-d redis \
redis-server /etc/redis/redis.conf

```

**注意：**redis.conf中daemonize不能改成yes,否则无法启动成功



### 安全问题

在平时的自己服务器上，我们可能会用root的方式启动，并将bind-ip配置成了0.0.0.0，而且没有配置密码，这个时候人家黑客就会容易进行攻击。这个是别人的博客说明：https://blog.csdn.net/weixin_43590389/article/details/109259526。

嗯哼，亲自遇到Kirito666,能不暴露在公网就不要暴露，暴露了就要指定ip,最差也得设置一个强度高的密码







