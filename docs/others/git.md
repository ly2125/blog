## 设置用户名和邮箱
```
git config --global user.name "my-name"
git config --global user.email "my-email"
```

设置 后，我们可以用`git config -l`查看配置

```
$ git config -l
diff.astextplain.textconv=astextplain
filter.lfs.clean=git-lfs clean -- %f
filter.lfs.smudge=git-lfs smudge -- %f
filter.lfs.process=git-lfs filter-process
filter.lfs.required=true
http.sslbackend=schannel
core.autocrlf=true
core.fscache=true
core.symlinks=false
pull.rebase=false
credential.helper=manager-core
credential.https://dev.azure.com.usehttppath=true
init.defaultbranch=main
gui.recentrepo=D:/study-demo
user.name=luyi
user.email=893431435@qq.com
```

## 访问githup过慢的解决办法

### 置Hosts

在ipaddress中查找github.com与fastly.net对应的ip，配置到hosts即可。

> https://www.ipaddress.com/

```go
## 这个是2021-05-22的ip,最好先看下IP地址是否正确
199.232.69.194 fastly.net                                                                                                                  
140.82.114.4 github.com
```

不同的操作系统的hosts文件不同：

- deeplin为/etc/hosts
