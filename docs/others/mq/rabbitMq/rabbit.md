## 文档

官网文档：https://www.rabbitmq.com/getstarted.html

## 安装

### 安装包安装

见：https://mp.csdn.net/editor/html/101868783

### docker安装

#### 下载镜像

进入docker hub镜像仓库地址：https://hub.docker.com/  ,我们选择带有“mangement”的版本（包含web管理页面）的镜像：

```bash
docker pull rabbitmq:management
```

在docker hup的对应的描述中有对应的版本

![image-20210220175049444](https://luyi-docs.oss-cn-chengdu.aliyuncs.com/blog/20210220175058.png)



  下载完成后，我们可以用docker images 查看镜像是否拉取成功  

#### 安装并启动web

##### 运行rabbitMq

```
docker run -d --name rabbitmq -p 5672:5672 -p 15672:15672 \
 -v `pwd`/data:/var/lib/rabbitmq --hostname my-rabbit \
 -e RABBITMQ_DEFAULT_VHOST=test \
 -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin \
 rabbitmq:management
```

rabbitmq:management为镜像的名称

说明：

- -d 后台运行容器；
- --name 指定容器名；
- -p 端口映射（5672：应用访问端口；15672：控制台Web端口号；由于我本地的这两个端口已经被使用，这里将5673映射到容器的5672，15673映射到容器的15672）；
- -v 映射目录或文件；
- --hostname 主机名（RabbitMQ的一个重要注意事项是它根据所谓的 “节点名称” 存储数据，默认为主机名）；
- -e 指定环境变量；（RABBITMQ_DEFAULT_VHOST：默认虚拟机名；RABBITMQ_DEFAULT_USER：默认的用户名；RABBITMQ_DEFAULT_PASS：默认用户名的密码）。ps:如果不设置，默认用户名和密码为`guest`/ `guest`

注意：3.9后上述 -e的参数已经不再使用


我们可以通过`docker logs` 和`docker ps`来查看是否启动成功

更多的启动配置我们见docker hup介绍：https://hub.docker.com/_/rabbitmq?tab=description&page=1&ordering=last_updated

##### 启动rabbitmq_management

进入rabbitmq的容器

```bash
 docker exec -it   rabbitmq bash
```

启动rabbitmq_management

```
rabbitmq-plugins enable rabbitmq_management
```

##### 测试

 输入：http://ip://15673 ,用户名和密码为admin,进入web端

![image-20210220182248303](https://luyi-docs.oss-cn-chengdu.aliyuncs.com/blog/20210220182249.png)



##  rabbitMq的queue类型



### 简单





## 自动签收和公平策略