##  rabbitMq可靠消息传递

### 生产者确认（项目没使用，暂时不整理）



### 消费者确认

在rabbitmq中，有三种确认方式：

- none 自动确认（默认的确认方式）
- manual 手动确认
- auto 根据异常情况确认

自动确认是指，当消息一旦被Consumer接收到，则自动确认收到，并将相应 message 从 RabbitMQ 的消息缓存中移除。但是在实际业务处理中，很可能消息接收到，业务处理出现异常（代码异常、服务跌机等），那么该消息就会丢失。如果设置了手动确认方式，则需要在业务代码中手动处理消息。下边是手动确认处理的几种方式：

- channel.basicAck()    消息确认，消息会将队列中移除。

- channel.basicReject(deliveryTag,requeue) 

  拒绝消息。 拒绝deliveryTag对应的消息，第二个参数是否requeue，true则重新入队列，否则丢弃或者进入死信队列。该方法reject后，该消费者还是会消费到该条被reject的消息。

- channel.basicNack(deliveryTag, multiple, requeue)   

  拒绝消息。和basicReject的区别就是支持批量拒绝消息（multiple为true表示拒绝<=deliveryTag的消息）。

- channel.basicRecover(true)

  是否恢复消息到队列，参数是是否requeue，true则重新入队列，并且尽可能的将之前recover的消息投递给其他消费者消费，而不是自己再次消费。false则消息会重新被投递给自己

#### **使用demo**

开启手动确认：

```
 rabbitmq:
    host: ${vm-host}
    port: 5672
    username: filing
    password: filing
    virtual-host: stats3
    listener:
      simple:
        ####开启手动ack
        acknowledge-mode: manual
```

消费者代码：

```
@RabbitListener(...)
public void process(Message message, Channel channel) throws IOException {
    try {
        //业务代码
        

        //消息确认
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    } catch (Exception e) {
        //消息拒绝,可以通过不同异常类型决定消息拒绝策略：1.进入死信队列、 2.丢失消息、3.重回对列
        //例如：连接异常重回队列
    } 
}
```

#### 注意

- 当配置了手动确认，一定要对消息确认，否则对应的消费者会一直阻塞；

- 对于重要的业务数据，建议开启手动签收，避免消息丢失

- 注意重复消费的问题

  例如：

  ```
    try {
          //业务代码  
        // 1. insert into user(id,name,password) values (uuid,'test','test');
        // 2.insert into user_info ..
      channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
  } catch (JDBCConnectionException e) {
       channel.basicRecover(true);
  } 
  ```


  ​        在这个代码，id为自动生成（或者自增），当步骤2的时候出现连接异常，消息就会重回对列。当消息再次消费的时候时候，就会再生成一条记录。

  



## 多个线程消费的问题

创建消费者代码

org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer#doStart

org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer#createBlockingQueueConsumer

一个RabbitListener默认是一个消费者，，一个消费者就是一个线程执行。也就是说默认单线程消费，可以配置多线程消费，可以通过RabbitListener注解中的concurrency配置多个消费者。每个消费者是一个阻塞队列，阻塞队列的大小为prefetchCount。例如：

```
##表示会创建10个消费者，10个线程消费
@RabbitListener(concurrency = "10")
```

rabbitMq出现大量消息堆积的时候会影响性能，当我们希望加快消费的时候可以使用该配置。

