## docker 安装nginx

### 拉取镜像

```
## 这里默认会拉取最新的镜像
docker pull  nginx
```

检查是否拉取成功，我们可以用docker images,查看时候有我们需要拉取的镜像。

###  从容器中拷贝需要的文件

一些静态的资源文件，或者配置文件，都在容器中，每次配置都要进入容器中，比较麻烦，这个时候我们可以拷贝中容器外，进行映射。

首先，我们启动nginx

```
docker run --name nginx -p 80:80 -d nginx
```

进入容器中，查看配置文件在哪儿

```
## 进入容器
>docker exec -it  nginx bash
## 查看nginx的位置
>whereis nginx
nginx: /usr/sbin/nginx /usr/lib/nginx /etc/nginx /usr/share/nginx
```

- /etc/nginx : 这个下边是nginx配置文件 
- /usr/share/nginx  这个是静态资源文件的目录

利用docker cp将配置文件和资源文件复制到容器外

```
docker  cp nginx:/etc/nginx/   /usr/local/docker/nginx/conf/
```

如果这个复制出来路径不是我们想要的，我们再利用cp,或者mv移动目录。



### 挂载启动

首先我们删除容器，

```
##停止容器
>docker stop nginx
##删除容器,注意rm是删除容器，rmi是删除镜像
>docker rm nginx
```

启动容器

```
docker run --name nginx -p 80:80   --restart=always  \
-v  /usr/local/docker/nginx/share:/usr/share/nginx \
-v /usr/local/docker/nginx/conf:/etc/nginx \
-d nginx
```

- `--restart=always` 表示docker启动的时候总是会自动nginx

### 验证

#### 验证是否启动成功

我们在浏览器中输入：http://localhost,如果看到nginx的页面，说明启动成功

#### 验证是否挂载成功

比如我们挂载在静态文件目录，我们只本地对应的目录中随便创建一个文件（或者文件夹），再进入nginx容器中，如果容器对应的目录有，就挂载成功啦。





## nginx 配置路由

```
http {
 upstream myproject {
     server 127.0.0.1:8000 weight=3;
     server 127.0.0.1:8001;
     server 127.0.0.1:8002;
     server 127.0.0.1:8003;
 }

server {
 listen 80;
 server_name www.domain.com;
 	location / { proxy_pass http://myproject;
 }
 }
}


stream {    # stream 模块配置和 http 模块在相同级别


    upstream 9848 {
        server 180.163.98.12:9848;
    }
    server {
        listen 9848 so_keepalive=on;
        proxy_connect_timeout 60s;
        proxy_timeout 60s;
        proxy_pass 9848;
		 
    }


}
```

