﻿@[toc]

# 文档
常用的命令参考：[https://www.runoob.com/docker/docker-command-manual.html](https://www.runoob.com/docker/docker-command-manual.html)
# 安装启动docker

`注意`：
 1. 以下是centerOs 用yum安装的docker,其他的liunx系统如Ubuntu用apt-get安装
 2. systemctl 的命令，其他用yum安装的软件也通用


 **let`s go**~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## 安装docker

```java
 yum install docker
```
 ## 启动docker

```java
systemctl start docker
```
 ## 开机启动docker
```java
 systemctl enable docker
```
## 禁止开机启动docker

```java
systemctl disable docker
```

 ## 查看docker是否启动

```java
systemctl status docker
```
  ##  停止docker

```java
 systemctl stop docker 
```
 ## 修改docker默认registry地址
 默认会从docker.io上下载镜像，可以如下方式改默认的registry地址：
修改/etc/docker目录下的daemon.json文件
在文件中加入

```java
{
  "registry-mirrors": ["https://registry.docker-cn.com"]
}
```
修改后记得重启docker: `systemctl restart docker`

# 镜像容器操作（安装mysql）
## 搜索镜像
```java
docker search mysql
```
 我们可以选择对应的mysql镜像安装

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200918124815279.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMyNzQ4ODY5,size_16,color_FFFFFF,t_70#pic_center)
## 拉取镜像

```java
docker pull  docker.io/mysql
```

我们可以到[https://hub.docker.com/](https://hub.docker.com/)，搜索mysql找到自己想要安装的版本进行安装，如果不指定版本就是安装`latest`最新的版本，安装指定版本的命令为:

```java
 docker pull  docker.io/mysql:{version}
```
## 查看镜像
docker images

## 删除镜像

```java
docker rmi [镜像名称或者Id]
```

删除所有的镜像用如下命令：

```java
##注意这个不是普通的单引号，是键盘esc下的引号
docker rmi `docker images -q`
```
注意：`当改镜像有运行的容器的时候，是无法删除的`
## 运行容器

```java
docker run -itd --name mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql
```

参数说明：

- -p 3306:3306 ：映射容器服务的 3306 端口到宿主机的 3306 端口，外部主机可以直接通过 宿主机ip:3306 访问到 MySQL 的服务。(第一个为3306为宿主机的端口，第二个为容器的端口）
- MYSQL_ROOT_PASSWORD=123456：设置 MySQL 服务 root 用户的密码
- 最后的mysql可以为image Id,也可以是REPOSITORY
- ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200919013322402.png#pic_center)
## 列出容器

```java
docker ps [OPTIONS]
```

OPTIONS说明：

注意：```当不加的时候表示列出运行的容器```

`-a :`显示所有的容器，包括未运行的。

-f :根据条件过滤显示的内容。

--format :指定返回值的模板文件。

-l :显示最近创建的容器。

-n :列出最近创建的n个容器。

--no-trunc :不截断输出。

-q :静默模式，只显示容器编号。

-s :显示总的文件大小。
## 进入容器

```java
docker exec -it 24fc70b178a9  /bin/bash
```


## 对运行（run过）的容器停止、启动

```java
docker start :启动一个或多个已经被停止的容器

docker stop :停止一个运行中的容器

docker restart :重启容器
```
## 删除容器

```java
##mysql-test为容器名称
docker rm mysql-test
```

# 设置容器自启动

每次电脑重启后，都 `docker start [容器]` 比较麻烦，这个时候我们就需要设置每次docker启动的时候，自动启动对应的容器

在运行docker容器时可以加如下参数来保证每次docker服务重启后容器也自动重启

```bash
docker run --restart=always
```

如果容器已经启动，我们就可以使用下边的命令

```bash
docker update --restart=always
```



# 文件拷贝

## 从主机拷贝到容器

```java
docker cp  test.txt  mysql-test:/usr/local/
```
test.txt 为主机的文件，mysql-test为容器的名称，/usr/local/为容器的目录

## 从容器拷贝到主机

```java
docker  cp mysql-test:/usr/local/test.txt  /usr/local
```

# docker 容器备份与恢复

## 容器保存为镜像

```java
docker commit mysql-test mysql-i
```
其中mysql-test为我需要备份的mysql容器，mysql-i为我备份镜像的名称

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200920004726418.png#pic_center)
我们会发现，利用`docker images`命令，会发现我们备份的镜像

## 镜像备份

```java
docker save -o mysql.tar mysql-i
```
o代表output输出的意思
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020092000520897.png#pic_center)

## 还原备份

```java
docker load -i mysql.tar
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200920010028789.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMyNzQ4ODY5,size_16,color_FFFFFF,t_70#pic_center)
删去原来的镜像，然后还原，我们会发现，还原的给原来的镜像名称一样



# 配置阿里云镜像加速

## 在控制台找到教程和加速地址
登陆阿里云进入控制台->容器镜像服务->镜像中心->镜像加速器。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20201129162541888.png)

## 配置加速
打开daemon.json

```java
vim /etc/docker/daemon.json
```
重启docker daomon线程

```java
systemctl daemon-reload
```
重启docker 服务

```java
systemctl restart docker
```