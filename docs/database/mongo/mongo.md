## docker 安装mongo

### 安装镜像

到docker hup 找到镜像（注意系统要对应，liunx要安装liunx的镜像）,这里安装4.2.12版本为例：

```bash
docker pull mongo:4.2.12
```

我们可以用`docker  images`查看镜像是否拉取成功

```basic
[root@hecs-x-medium-2-linux-20200702090745 ~]# docker images
REPOSITORY                      TAG                 IMAGE ID            CREATED             SIZE
docker.io/mongo                 4.2.12              a554fe3e4b7f        6 hours ago         388 MB
```

### 启动容器

```bash
docker run -itd --name mongo -p 27017:27017 mongo:4.2.12 --auth
```

其中 --auth表示需要权限验证

配置目录挂载:
docker run -itd --name mongo -p 27017:27017 \
-v /usr/local/soft/docker/mongo/data:/data/db \
-v /usr/local/soft/docker/mongo/config:/data/configdb \
mongo:4.2.12 --auth

### 创建mongoDb用户

#### 创建管理用户

```
## 进入容器
>docker exec -it mongo bash
## 进入mongo命令窗口
>mongo
# 进入admin数据库
>use admin
 switched to db admin
>db.createUser({user:"admin",pwd:"admin",roles:[{role:"root",db:"admin"}]});
Successfully added user: {
  "user" : "admin",
  "roles" : [
    {
      "role" : "root",
      "db" : "admin"
    }
  ]
}
>exit
```

或者使用下边的命令直接进入admin数据库

```bash
docker exec -it mongo mongo admin
```

#### 创建普通用户

```
## 用刚创建的管理用户登录在创建个普通用户,记得exit上步操作。
>mongo --port 27017 -u admin -p admin --authenticationDatabase admin
use test
switched to db test
db.createUser({user:"tester",pwd:"tester",roles:[{role:"readWrite",db:"test"}]});
Successfully added user: {
  "user" : "tester",
  "roles" : [
    {
      "role" : "readWrite",
      "db" : "test"
    }
  ]
}
## 认证
> db.auth('test','test')

exit
```

注意：创建普通用户都需要先用普通用户登录验证后再操作

#### 数据库角色说明

mongoDb用户角色权限说明

- 数据库用户角色 read、readWrite
- 数据库管理角色 dbAdmin、dbOwner、userAdmin
- 集群管理角色 clusterAdmin、clusterManager、clusterMonitor、 hostManager
- 备份恢复角色 backup、restore
- 所有数据库角色 readAnyDatabase、readWriteAnyDatabase、userAdminAnyDatabase、dbAdminAnyDatabase
- 超级用户角色 root
- 内部角色 __system

角色说明

- Read 允许用户读取指定数据库
- readWrite 允许用户读写指定数据库
- dbAdmin 允许用户在指定数据库中执行管理函数，如索引创建、删除，查看统计或访问system.profile
- userAdmin 允许用户向system.users集合写入，可以找指定数据库里创建、删除和管理用户
- clusterAdmin 只在admin数据库中可用，赋予用户所有分片和复制集相关函数的管理权限。
- readAnyDatabase 只在admin数据库中可用，赋予用户所有数据库的读权限
- readWriteAnyDatabase 只在admin数据库中可用，赋予用户所有数据库的读写权限
- userAdminAnyDatabase 只在admin数据库中可用，赋予用户所有数据库的userAdmin权限
- dbAdminAnyDatabase 只在admin数据库中可用，赋予用户所有数据库的dbAdmin权限。
- root 只在admin数据库中可用。超级账号，超级权限

## 数据备份与恢复

平时在开发和测试环境中，我们可能都是使用的数据库工具进行备份和恢复。但是在生产环境上，有时候我们的客户端是无法连接数据库的，这个时候我们就需要使用命令导出。

### 备份单个集合

> mongoexport -u filing -p filing --collection=stats_gathered_respondent_report_task --db=stats3-filing --out=stats_gathered_respondent_report_task.json