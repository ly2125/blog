

##  查询语法

​	[参考地址](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/search-search.html)

```http
GET /<target>/_search

GET /_search

POST /<target>/_search

POST /_search
```

我们可以在地址后拼接参数或者requestBody这两种方式查询

账户的钱按照降序排列，示例：

```
GET /bank/_search?q=*&sort=account_number:desc
```

```json
GET /bank/_search
{
  "query": {
   "match_all": {}
  },
  "sort": [
    {
      "account_number":{
        "order": "desc"
      }
    }
  ]
}
```

###  请求参数



 对于更多的参数看官网文档的解释,下边列举几个常用的

- `_source` ：查询的字段，类似mysql中不用select * ,查询具体的字段
- `from`:  和**size**用来分页，类型mysql中的limit。默认情况下，您不能使用`from` 和`size`参数分页浏览超过10,000个文档。使用[`index.max_result_window`](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/index-modules.html#index-max-result-window)索引设置来设置此限制 。深度分页或一次请求许多结果可能会导致搜索缓慢。结果在返回之前先进行排序。由于搜索请求通常跨越多个分片，因此每个分片必须生成自己的排序结果。然后必须对这些单独的结果进行合并和排序，以确保总体顺序正确。作为深度分页的替代方法，我们建议使用 [scroll](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/paginate-search-results.html#scroll-search-results)或 [`search_after`](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/paginate-search-results.html#search-after)参数。
- `sort`:排序
- `version`：（可选，布尔值）如果为`true`，则返回文档版本，作为匹配的一部分。
- `query`:（可选，[查询对象](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/query-dsl.html)）使用[查询DSL](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/query-dsl.html)定义搜索定义 。

### 响应参数(待整理)



##  全文查询

###  match （单字段匹配）

- match 会将关键词进行分词（`字符串类型`），查找时包含其中任一均可被匹配到，适合模糊匹配；
- 查询的结果会按照max_score得到从高到底排序；

例如：

```http
GET /bank/_search
{
  "query": {
    "match": {
      "address": "498 Laurel Avenue"
    }
  }
}
```

"498 Laurel Avenue"会被分成“498”，“Laurel”，“Avenue”，然后再进行匹配。





**拓展**：如果需要用match精确查找（也就是不分词匹配），可以用keyword

```
GET /bank/_search
{
  "query": {
    "match": {
      "address.keyword": "498 Laurel Avenue"
    }
  }
}
```



### match_phrase（精确查询）

和match的区划就是match_phrase不会进行分词，适合精确查找。

例如：

```
GET /bank/_search
{
  "query": {
    "match_phrase": {
      "address": "498 Laurel Avenue"
    }
  }
}
```

只会查询出`address`为`498 Laurel Avenue`的数据



### multi_match（多字段匹配）

多个字段匹配,其他的和match一样。

```http
{
  "query": {
    "multi_match": {
      "query": "查询的值",
      "fields": []
    }
  }
}
```

将查询的字段`query`进行分词，分词后和`fields`对应的字段的值进行匹配。

### match_all

匹配所有

```
GET /bank/_search
{
  "query": {
    "match_all": {}
  }
}
```

 

### term

​		term是代表完全匹配，也就是精确查询，搜索前不会再对搜索词进行分词，所以我们的搜索词必须是文档分词集合中的一个(字段值完全匹配，包括空格和大写字母)。下边是官方的话：

>
> 避免`term`对[`text`](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/text.html)字段使用查询。
>
> 默认情况下，Elasticsearch更改`text`字段的值作为[analysis的](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/analysis.html)一部分。这会使查找`text`字段值的精确匹配变得困难。

要搜索`text`字段值，请改用[`match`](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/query-dsl-match-query.html)查询。

所以，我们应该用它查询精确的值（例如价格，产品ID或用户名）。

## bool （复合查询）

​     用于组合多个叶或化合物查询子句，可以结合 `must`，`should`，`must_not`，或`filter`使用。在`must`和`should` 查询的时候会提高分数，而`must_not`和`filter`不会影响分数（分数影响排序）。

| 布尔型   | 描述                                                         |
| -------- | ------------------------------------------------------------ |
| must     | 子句（查询）必须出现在匹配的文档中，并将有助于得分。         |
| filter   | 子句（查询）必须出现在匹配的文档中。但是不像 `must`查询的分数将被忽略。Filter子句在[filter上下文](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/query-filter-context.html)中执行，这意味着计分被忽略，并且子句被考虑用于缓存。 |
| should   | 子句（查询）应出现在匹配的文档中（备注：满足了会提高分数）。 |
| must_not | 子句（查询）不得出现在匹配的文档中。子句在[过滤器上下文](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/query-filter-context.html)中执行，这意味着计分被忽略，而子句被视为用于缓存。因为忽略计分，`0`所以将返回所有文档的分数。 |

该`bool`查询采用的*是“更好匹配”的*方法，因此每个匹配项`must`或`should`子句的得分将加在一起，以提供`_score`每个文档的最终结果。

参考：[官网文档7.9](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/query-dsl-bool-query.html)