
## 下载镜像
```bash
docker pull kibana:7.9.3
```
注意：我们使用kibana的时候，对应tags应该和es对应

## 启动

```bash
docker run  --name  kibana -e ELASTICSEARCH_HOSTS=http://192.168.0.57:9200 \
 -p 5601:5601 -d  kibana:7.9.3
```




启动的参数见[docker hup地址](https://www.elastic.co/guide/en/kibana/current/docker.html)

