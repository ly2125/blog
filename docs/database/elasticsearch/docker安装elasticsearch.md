## 下载镜像文件

找到对应版本的tags,拉取镜像，这里安装7.9.3。[地址点我](https://hub.docker.com/_/elasticsearch?tab=tags&page=1&ordering=last_updated)

```bash
docker pull elasticsearch:7.9.3
```

注意：如果需要可视化工具kibana，版本也需要对应


 我们可以用docker images的命令，查看对应的镜像是否拉取成功

```bas
docker images
```



##  创建挂载文件

在机器上创建文件夹，分别保存es的数据保存文件、配置文件和插件，我这里创建的data 、config、plugins

```bash
[root@hecs-x-medium-2-linux-20200702090745 elasticsearch]# ls
config  data  plugins
```

然后在conf中创建elasticsearch.yml的配置文件（进入conf中，用touch elasticsearch.yml命令创建），并增加如下配置：

```bash
## 允许任何IP访问
http.host: 0.0.0.0
```

### 利用容器生成挂载文件

一般的情况下，我们可以启动一个没有挂载文件的容器，然后从容器中把默认的配置文件拷贝出来（docker cp命令），再挂载，下边是es默认的目录：

```
 bin    config  data  jdk  lib  logs  modules  plugins
```

进入config,可以看到如下，我们就可以进行配置

```
elasticsearch.keystore  elasticsearch.yml  jvm.options  jvm.options.d log4j2.properties  role_mapping.yml  roles.yml  users  users_roles
```



## 启动

```bash
docker run -p 9200:9200 -p 9300:9300 --name es \
   -e "discovery.type=single-node" \
   -e "cluster.name=elasticsearch" \
   -e ES_JAVA_POTS="-Xms128m -Xmx128m" \
   -v /usr/local/docker/elasticsearch/plugins:/usr/share/elasticsearch/plugins \
   -v /usr/local/docker/elasticsearch/data:/usr/share/elasticsearch/data \
   -v /usr/local/docker/elasticsearch/config:/usr/share/elasticsearch/config \
   -d elasticsearch:7.9.3
```

- 9200是 HTTP 协议的 RESTful 接口;

- 9300 是 TCP 通讯端口，集群间和 TCPClient 都走得它;

- --name 表示启动容器的名称

- `discovery.type=single-node` 表示单机启动

- `-e ES_JAVA_POTS="-Xms128m -Xmx128m"` 表示设置最小内存为128M,最大的内存为256M,不设置的时候默认为1G（哈哈，自己的电脑配置跟不上，就加上这个）。当然这个也可以在jvm.options中配置

- -v 文件挂载，方便我们在容器外操作配置文件或者插件

- -d 表示后台启动



## 验证启动成功

  1. 用`docker ps` ，能够找到对应的容器

  2. 访问http://对应ip:9200,如果看到以下内容，就说明启动成功。

     ```json
     {
       "name" : "e87076c7b9c6",
       "cluster_name" : "elasticsearch",
       "cluster_uuid" : "fLQ_6TpMRFS6Cgi0nfDINw",
       "version" : {
         "number" : "7.9.3",
         "build_flavor" : "default",
         "build_type" : "docker",
         "build_hash" : "c4138e51121ef06a6404866cddc601906fe5c868",
         "build_date" : "2020-10-16T10:36:16.141335Z",
         "build_snapshot" : false,
         "lucene_version" : "8.6.2",
         "minimum_wire_compatibility_version" : "6.8.0",
         "minimum_index_compatibility_version" : "6.0.0-beta1"
       },
       "tagline" : "You Know, for Search"
     }
     ```


## 常见启动错误解决办法整理    

当无法启动的时候，我们可以利用`docker logs  [容器名称]` 查看日志信息

###  java.nio.file.AccessDeniedException: /usr/share/elasticsearch/data/nodes"

我们到挂载目录用`ll`,会发现没有读写执行的权限

```bash
[root@hecs-x-medium-2-linux-20200702090745 elasticsearch]# ll
总用量 12
drwxr-xr-x 2 root root 4096 1月  24 16:40 conf
drwxr-xr-x 2 root root 4096 1月  24 16:36 data
drwxr-xr-x 2 root root 4096 1月  24 17:02 plugins
```

我们需要执行如下命令，给与权限