mapping也就是字段定义信息，就像mysql中字段为int,varchar一样

## 查看mapping

语法

```http
GET  /{索引}/_mapping
```

例如，我们为了查看bank的mapping,我们可以：`GET /bank/_mapping`，返回结果如下,我们可以看到每个字段的信息：

```json
{
  "bank" : {
    "mappings" : {
      "properties" : {
        "account_number" : {
          "type" : "long"
        },
        "address" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "age" : {
          "type" : "long"
        },
        "balance" : {
          "type" : "long"
        },
        "city" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "email" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "employer" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "firstname" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "gender" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "lastname" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "state" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        }
      }
    }
  }
}
```

## 创建mapping

我们在批量导入bank数据的时候，发现es会自动我们生成字段的类型，但是自动生成的有时候并不是我们想要的，所以我们需要在之前就创建好字段的类型。

具体有哪些字段类型，见[官网文档](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/mapping-types.html)

例如，我们需要将age定义为整形，email定义为字符串精确匹配，name定义为text，我们可以：

```console
PUT /my-index
{
  "mappings": {
    "properties": {
      "age":    { "type": "integer" },  
      "email":  { "type": "keyword"  }, 
      "name":   { "type": "text"  }     
    }
  }
}
```

##  添加新的字段映射

有时候，字段的数量不满足我们的要求，我们需要新增一个字段。比如，我们需要增加一个qq(keyword类型)

```json
PUT /my-index/_mapping
{
  "properties": {
    "qq":    { "type": "keyword" }
  }
}
```

## 修改字段映射&数据迁移

在很多时候，我们需要我们的字段类型定义的有问题，我们想要修改字段的类型。但是在es中，并不能直接修改字段类型，更改现有字段可能会使已经建立索引的数据无效。我们只能只用reindex(将文档从*源*复制到目标),文档地址：[reindex](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/docs-reindex.html)

```console
POST _reindex
{
  "source": {
    "index": "my-index-000001"
  },
  "dest": {
    "index": "my-new-index-000001"
  }
}
```

当我们需要同时修改多个索引的时候，我们可以这样：

```bash
for index in i1 i2 i3 i4 i5; do
  curl -HContent-Type:application/json -XPOST localhost:9200/_reindex?pretty -d'{
    "source": {
      "index": "'$index'"
    },
    "dest": {
      "index": "'$index'-reindexed"
    }
  }'
done
```