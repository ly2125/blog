## 基于二进制日志（binlog）

mariadb官网主从搭建地址：https://mariadb.com/kb/en/setting-up-replication/

### 主配置

将以下内容添加到[my.cnf](https://mariadb.com/kb/en/configuring-mariadb-with-mycnf/)文件中并重新启动数据库。

```
[mariadb] 
log-bin 
server_id=1 
## MySQL 不支持log-basename.
log-basename=master1 
binlog-format=mixed
```

服务器 ID 是网络中每个 MariaDB/MySQL 服务器的唯一编号。 [binlog-format](https://mariadb.com/kb/en/binary-log-formats/)指定如何记录您的语句。这主要影响Master 和 Slaves 之间发送的[二进制日志](https://mariadb.com/kb/en/binary-log/)的大小。

 注意，对于mariadb也可以配置在对应的配置文件中，如在mariadb的10.6的版本中，mariadb.conf.d下的 50-server.cnf

```
server-id              = 1
log_bin                = /var/log/mysql/mysql-bin.log
expire_logs_days        = 10
```

然后，添加主从同步的用户，并授权：

```
CREATE  USER  'replication_user' @ '%'  IDENTIFIED  BY  'bigs3cret' ; 
GRANT  REPLICATION  SLAVE  ON  * 。*  TO  'replication_user' @ '%' ;
```

### 从配置

#### 获取主库的二进制日志位置

通过运行获取二进制日志中的当前位置`SHOW MASTER STATUS`：

| FIle             | Position | Binlog_Do_DB | Binlog_ignore_DB |
| :--------------- | -------- | ------------ | ---------------- |
| mysql-bin.000001 | 2534     |              |                  |

- file binLog的文件名

- Position位置

- Binlog_Do_DB 此参数表示只记录指定数据库的二进制日志，默认全部记录

- Binlog_ignore_DB 参数表示不记录指定的数据库的二进制日志。

  ```mysql
  # 指定 db1 db2 记录binlog
  [mysqld]
  binlog_do_db = db1
  binlog_do_db = db2
  
  # 不让 db3 db4 记录binlog
  [mysqld]
  binlog_ignore_db = db3
  binlog_ignore_db = db4
  ```

注意，在配置的期间，防止主库数据修改，我们首先可以锁定主库：`FLUSH TABLES WITH READ LOCK`，然后配置完成后，再取消锁定：`UNLOCK TABLES;`

#### 启动复制

```
CHANGE  MASTER  TO 
  MASTER_HOST = 'master.domain.com' ，
  MASTER_USER = 'replication_user' ，
  MASTER_PASSWORD = 'bigs3cret' ，
  MASTER_PORT = 3306 ，
  MASTER_LOG_FILE = 'master1-bin.000096' ，
  MASTER_LOG_POS = 568 ，
  MASTER_CONNECT_RETRY = 10 ;
```

```
## 启动从库
start slave;
```



#### 检查是否配置成功

```
##\G让我们能够看到数据是有格式的
SHOW SLAVE STATUS \G
```

如果复制工作正常，`Slave_IO_Running`和`Slave_SQL_Running`应为`Yes`：

```
Slave_IO_Running：Yes
Slave_SQL_Running：Yes
```

## 基于全局事务 ID（ GTID）

mariadb官网文档：https://mariadb.com/kb/en/gtid/

[MariaDB 10.0](https://mariadb.com/kb/en/what-is-mariadb-100/)引入了用于复制的全局事务 ID (GTID)。通常建议使用[MariaDB 10.0 中的](https://mariadb.com/kb/en/what-is-mariadb-100/)(GTID) ，因为这有很多好处。所需要做的就是将`MASTER_USE_GTID`选项添加到`CHANGE MASTER`语句中，例如：

```
CHANGE  MASTER  TO  MASTER_USE_GTID  =  slave_pos
```