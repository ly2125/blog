示例数据：

```mysql
CREATE TABLE employees (
    id INT ( 11 ) NOT NULL AUTO_INCREMENT,
    NAME VARCHAR ( 24 ) NOT NULL DEFAULT '' COMMENT '姓名',
    age INT ( 11 ) NOT NULL DEFAULT '0' COMMENT '年龄',
    position VARCHAR ( 20 ) NOT NULL DEFAULT '' COMMENT '职位',
    hire_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '入职时间',
    PRIMARY KEY ( id ),
    KEY idx_name_age_position ( NAME, age, position ) USING BTREE 
) ENGINE = INNODB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8 COMMENT = '员工记录表';
INSERT INTO employees ( NAME, age, position, hire_time )
VALUES
    (
        'LiLei',
        22,
        'manager',
    NOW());
INSERT INTO employees ( NAME, age, position, hire_time )
VALUES
    (
        'HanMeimei',
        23,
        'dev',
    NOW());
INSERT INTO employees ( NAME, age, position, hire_time )
VALUES
    (
        'Lucy',
        23,
        'dev',
    NOW());
‐‐ 插入一些示例数据
DROP PROCEDURE IF EXISTS insert_emp;

delimiter;;
CREATE PROCEDURE insert_emp () BEGIN
    DECLARE
        i INT;
    
    SET i = 1;
    WHILE
            ( i <= 100000 ) DO
            INSERT INTO employees ( NAME, age, position )
        VALUES
            ( CONCAT( 'zhuge', i ), i, 'dev' );
        
        SET i = i + 1;
        
    END WHILE;
    
END;;

delimiter;
CALL insert_emp ();
```

### ****联合索引第一个字段用范围不会走索引****
```mysql
EXPLAIN SELECT * FROM employees WHERE name > 'LiLei' AND age = 22 AND position ='manager';
```
![mysql查询优化](images/mysql查询优化/联合索引第一个字段用范围不会走索引.jpg)
结论：联合索引第一个字段就用范围查找不会走索引，mysql内部可能觉得第一个字段就用范围，结果集应该很大，回表效率不高，还不 如就全表扫描
