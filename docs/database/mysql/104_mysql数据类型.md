mysql数据类型官方说明8.0:https://dev.mysql.com/doc/refman/8.0/en/data-types.html

##  数字类型

###  整数类型（精确值）

MySQL 支持 SQL 标准整数类型 `INTEGER`（或`INT`）和 `SMALLINT`. 作为一个可扩展标准，MySQL也支持整数类型 `TINYINT`，`MEDIUMINT`和 `BIGINT`。下表显示了每种整数类型所需的存储和范围。

支持的整数类型所需的存储和范围

| 类型        | 存储（字节） | 有符号的最小值 | 最小值无符号 | 最大值有符号 | 最大值无符号 |
| :---------- | :----------- | :------------- | :----------- | :----------- | :----------- |
| `TINYINT`   | 1            | `-128`         | `0`          | `127`        | `255`        |
| `SMALLINT`  | 2            | `-32768`       | `0`          | `32767`      | `65535`      |
| `MEDIUMINT` | 3            | `-8388608`     | `0`          | `8388607`    | `16777215`   |
| `INT`       | 4            | `-2147483648`  | `0`          | `2147483647` | `4294967295` |
| `BIGINT`    | 8            | `-263`         | `0`          | `263-1`      | `264-1`      |

### 定点类型（精确值）- DECIMAL、NUMERIC

该`DECIMAL`和`NUMERIC` 类型的存储精确的数值数据。这些类型用于保持精确精度很重要的情况，例如货币数据。在MySQL中，`NUMERIC`被实现为`DECIMAL`，所以下面的话大约`DECIMAL`同样适用于 `NUMERIC`。

MySQL`DECIMAL`以二进制格式存储值。请参见[第 12.25 节，“精确数学”](https://dev.mysql.com/doc/refman/8.0/en/precision-math.html)。

在`DECIMAL`列声明中，可以（并且通常是）指定精度和小数位数。例如：

```sql
salary DECIMAL(5,2)
```

在本例中，`5`是精度， `2`是比例。精度表示为值存储的有效位数，小数位数表示可以存储在小数点后的位数。

标准 SQL 要求`DECIMAL(5,2)`能够存储具有五位数字和两位小数的任何值，因此可以存储在`salary` 列范围内的值从`-999.99`到 `999.99`.

在标准 SQL 中，语法 等效于 . 类似地，语法等价于，其中允许实现决定 的值 。MySQL 支持这两种变体形式的语法。的默认值为10。 `DECIMAL(*`M`*)``DECIMAL(*`M`*,0)``DECIMAL``DECIMAL(*`M`*,0)`*`M`*`DECIMAL`*`M`*

如果`DECIMAL`小数位数为 0，则值不包含小数点或小数部分。

的最大位数为`DECIMAL`65，但给定`DECIMAL` 列的实际范围可能受给定列的精度或小数位数限制。如果为此类列分配的值的小数点后位数超过指定比例所允许的位数，则该值将转换为该比例。（确切的行为是特定于操作系统的，但通常效果是截断到允许的位数。）

### 浮点类型（近似值）- FLOAT、DOUBLE

`FLOAT`和`DOUBLE`类型代表近似数字数据值。MySQL 对单精度值使用四个字节，对双精度值使用八个字节。关于浮点型的说明我们可以查看官网：[浮点型问题](https://dev.mysql.com/doc/refman/8.0/en/problems-with-float.html)

### 位值类型

mysql中有几种存储类型使用紧凑的位存储引擎。所有的这些的位类型，不管底层存储格式，还是处理方式，从技术来说，都是字符串类型：[位值文字mysql说明](https://dev.mysql.com/doc/refman/8.0/en/bit-value-literals.html)

#### bit

#### set



### 数字类型属性

#### 指定整数数据类型的显示宽度和ZEROFILL

[`INT(4)`](https://dev.mysql.com/doc/refman/8.0/en/integer-types.html)指定 [`INT`](https://dev.mysql.com/doc/refman/8.0/en/integer-types.html)显示宽度为四位数的（和ZEROFILL使用才行，前边补0，但是并不能限定整数型的长度为4）

当与可选（非标准）`ZEROFILL`属性结合使用时 ，空格的默认填充将替换为零。例如，对于声明为 的列[`INT(4) ZEROFILL`](https://dev.mysql.com/doc/refman/8.0/en/integer-types.html)， 的值`5`检索为 `0005`。

> `ZEROFILL`对于表达式或[`UNION`](https://dev.mysql.com/doc/refman/8.0/en/union.html)查询中涉及的列 ，将忽略该属性 。
>
> 如果在具有该`ZEROFILL` 属性的整数列中存储大于显示宽度的值，则当 MySQL 为某些复杂连接生成临时表时，您可能会遇到问题。在这些情况下，MySQL 假定数据值适合列显示宽度。

#### UNSIGNED

浮点和定点类型也可以是 `UNSIGNED`. 与整数类型一样，此属性可防止将负值存储在列中。与整数类型不同，列值的上限范围保持不变。从 MySQL 8.0.17 开始，该 `UNSIGNED`属性对于类型为[`FLOAT`](https://dev.mysql.com/doc/refman/8.0/en/floating-point-types.html)、 [`DOUBLE`](https://dev.mysql.com/doc/refman/8.0/en/floating-point-types.html)和 [`DECIMAL`](https://dev.mysql.com/doc/refman/8.0/en/fixed-point-types.html)（以及任何同义词）的列已弃用，您应该期望在未来版本的 MySQL 中删除对它的支持。考虑`CHECK`对此类列使用简单 约束。

如果您`ZEROFILL`为数字列指定，MySQL 会自动添加该`UNSIGNED` 属性。



### 超出范围和溢出处理

当 MySQL 在数值列中存储超出列数据类型允许范围的值时，结果取决于当时生效的 SQL 模式：

- 如果启用了严格的 SQL 模式，MySQL 会根据 SQL 标准拒绝超出范围的值并显示错误，插入失败。

- 如果没有启用限制模式，MySQL 会将值剪辑到列数据类型范围的适当端点并存储结果值。

  当超出范围的值分配给整数列时，MySQL 存储表示列数据类型范围的相应端点的值。

  当为浮点或定点列分配的值超出指定（或默认）精度和小数位数所隐含的范围时，MySQL 存储表示该范围对应端点的值。

```
## 开启严格sql模式
SET sql_mode = 'TRADITIONAL';
## 关闭严格sql模式
SET sql_mode = '';
```

## 日期和时间数据类型

| 数据类型                                                     | “零”值                  |
| :----------------------------------------------------------- | :---------------------- |
| [`DATE`](https://dev.mysql.com/doc/refman/8.0/en/datetime.html) | `'0000-00-00'`          |
| [`TIME`](https://dev.mysql.com/doc/refman/8.0/en/time.html)  | `'00:00:00'`            |
| [`DATETIME`](https://dev.mysql.com/doc/refman/8.0/en/datetime.html) | `'0000-00-00 00:00:00'` |
| [`TIMESTAMP`](https://dev.mysql.com/doc/refman/8.0/en/datetime.html) | `'0000-00-00 00:00:00'` |
| [`YEAR`](https://dev.mysql.com/doc/refman/8.0/en/year.html)  | `0000`                  |

### DATE、DATETIME 和 TIMESTAMP 类型

#### 支持范围

该`DATE`（3个字节）类型用于具有日期部分但没有时间部分的值。MySQL`DATE`以 格式检索和显示 值 。支持的范围是 到。 `'*`YYYY-MM-DD`*'``'1000-01-01'``'9999-12-31'`

该`DATETIME`（8个字节）类型用于同时包含日期和时间部分的值。MySQL`DATETIME`以格式检索和显示 值 。支持的范围是 到。 `'*`YYYY-MM-DD hh:mm:ss`*'``'1000-01-01 00:00:00'``'9999-12-31 23:59:59'`

该`TIMESTAMP`（4个字节）数据类型被用于同时包含日期和时间部分的值。 `TIMESTAMP`有一个`'1970-01-01 00:00:01'`UTC 到`'2038-01-19 03:14:07'`UTC的范围。



#### TIMESTAMP 和 DATETIME 的自动初始化和更新

[mysql官方说明](https://dev.mysql.com/doc/refman/8.0/en/timestamp-initialization.html)

[`TIMESTAMP`](https://dev.mysql.com/doc/refman/8.0/en/datetime.html)和 [`DATETIME`](https://dev.mysql.com/doc/refman/8.0/en/datetime.html)列可以自动初始化并更新为当前日期和时间（即当前时间戳）。

对于表中的任何[`TIMESTAMP`](https://dev.mysql.com/doc/refman/8.0/en/datetime.html)或 [`DATETIME`](https://dev.mysql.com/doc/refman/8.0/en/datetime.html)列，您可以将当前时间戳指定为默认值、自动更新值或两者：

- 自动初始化的列被设置为未指定列值的插入行的当前时间戳。
- 当行中任何其他列的值从其当前值更改时，自动更新的列会自动更新为当前时间戳。如果所有其他列都设置为其当前值，则自动更新的列保持不变。要防止自动更新的列在其他列更改时更新，请将其显式设置为其当前值。要在其他列未更改时更新自动更新的列，请将其显式设置为应具有的值（例如，将其设置为 [`CURRENT_TIMESTAMP`](https://dev.mysql.com/doc/refman/8.0/en/date-and-time-functions.html#function_current-timestamp)）



规则如下（具体看官网说明，跟 [`explicit_defaults_for_timestamp`](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_explicit_defaults_for_timestamp) 系统变量有一定联系）：

- 使用`DEFAULT CURRENT_TIMESTAMP`和 `ON UPDATE CURRENT_TIMESTAMP`，该列的默认值具有当前时间戳，并自动更新为当前时间戳。

  ```sql
  CREATE TABLE t1 (
    ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    dt DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  );
  ```

- 有`DEFAULT`子句但没有`ON UPDATE CURRENT_TIMESTAMP`子句时，列具有给定的默认值并且不会自动更新到当前时间戳。

  默认值取决于 `DEFAULT`子句是指定 `CURRENT_TIMESTAMP`值还是常量值。使用`CURRENT_TIMESTAMP`，默认为当前时间戳。

  ```sql
  CREATE TABLE t1 (
    ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    dt DATETIME DEFAULT CURRENT_TIMESTAMP
  );
  ```

  对于常量，默认值为给定值。在这种情况下，该列根本没有自动属性。

  ```sql
  CREATE TABLE t1 (
    ts TIMESTAMP DEFAULT 0,
    dt DATETIME DEFAULT 0
  );
  ```

- 使用`ON UPDATE CURRENT_TIMESTAMP` 子句和常量`DEFAULT`子句，列会自动更新为当前时间戳并具有给定的常量默认值。

  ```sql
  CREATE TABLE t1 (
    ts TIMESTAMP DEFAULT 0 ON UPDATE CURRENT_TIMESTAMP,
    dt DATETIME DEFAULT 0 ON UPDATE CURRENT_TIMESTAMP
  );
  ```

- 有`ON UPDATE CURRENT_TIMESTAMP` 子句但没有`DEFAULT`子句时，列会自动更新为当前时间戳，但没有默认值的当前时间戳。

  这种情况下的默认值是类型相关的。 [`TIMESTAMP`](https://dev.mysql.com/doc/refman/8.0/en/datetime.html)除非使用`NULL`属性定义，否则默认值为 0 ，在这种情况下，默认值为`NULL`。

  ```sql
  CREATE TABLE t1 (
    ts1 TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,     -- default 0
    ts2 TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP -- default NULL
  );
  ```

  [`DATETIME`](https://dev.mysql.com/doc/refman/8.0/en/datetime.html)有一个默认值， `NULL`除非用`NOT NULL`属性定义，在这种情况下，默认值是 0。

  ```sql
  CREATE TABLE t1 (
    dt1 DATETIME ON UPDATE CURRENT_TIMESTAMP,         -- default NULL
    dt2 DATETIME NOT NULL ON UPDATE CURRENT_TIMESTAMP -- default 0
  );
  ```

## 字符串数据类型

### CHAR 和 VARCHAR 类型

- 一个长度`CHAR`列被固定在创建表声明的长度。长度可以是 0 到 255 之间的任何值。`CHAR` 存储值时，它们会用空格右填充到指定的长度。当`CHAR`被检索到的值，拖尾的空格被删除，除非 [`PAD_CHAR_TO_FULL_LENGTH`](https://dev.mysql.com/doc/refman/8.0/en/sql-mode.html#sqlmode_pad_char_to_full_length)启用SQL模式

- `VARCHAR`列中的 值是可变长度的字符串。长度可以指定为 0 到 65,535 之间的值。a 的有效最大长度 `VARCHAR`受最大行大小（65,535 字节，在所有列之间共享）和使用的字符集的约束。与 相比`CHAR`， `VARCHAR`值存储为 1 字节或 2 字节长度的前缀加数据。长度前缀表示值中的字节数。如果值需要不超过 255 个字节，则列使用一个长度字节，如果值可能需要超过 255 个字节，则使用两个长度字节。

如果未启用严格 SQL 模式并且您为`CHAR`或`VARCHAR`列分配的值 超过该列的最大长度，则该值将被截断以适合并生成警告。对于非空格字符的截断，您可以使用严格的 SQL 模式导致发生错误（而不是警告）并禁止插入值。

对于`VARCHAR`列，超出列长度的尾随空格在插入之前被截断并生成警告，无论使用何种 SQL 模式。对于 `CHAR`列，无论 SQL 模式如何，都会以静默方式从插入的值中截断多余的尾随空格。

`VARCHAR`值在存储时不会被填充。根据标准 SQL，在存储和检索值时保留尾随空格。

下表说明之间的差别 `CHAR`和`VARCHAR`通过显示各种字符串值存储到的结果 `CHAR(4)`和`VARCHAR(4)` 列（假设该列使用单字节字符集，例如`latin1`）。

| 价值         | `CHAR(4)` | 需要存储 | `VARCHAR(4)` | 需要存储 |
| :----------- | :-------- | :------- | :----------- | :------- |
| `''`         | `'  '`    | 4字节    | `''`         | 1 字节   |
| `'ab'`       | `'ab '`   | 4字节    | `'ab'`       | 3 个字节 |
| `'abcd'`     | `'abcd'`  | 4字节    | `'abcd'`     | 5 字节   |
| `'abcdefgh'` | `'abcd'`  | 4字节    | `'abcd'`     | 5 字节   |

显示为存储在表最后一行的值*仅*适用 *于不使用严格 SQL 模式的情况*；如果启用了严格模式，则*不会存储*超过列长度的值 ，并导致错误

### BINARY 和 VARBINARY 类型

TODO

### BLOB 和 TEXT 类型

TODO

### ENUM 类型

TODO

### SET 类型

TODO



## 空间数据类型

[官方说明](https://dev.mysql.com/doc/refman/8.0/en/spatial-types.html)

TODO 

