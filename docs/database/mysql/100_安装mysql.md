## docker安装mariadb

### 拉取镜像

在dockerhup上选择合适的镜像，这里我们选择mariadb:10.6.1的版本

```bash
docker pull mariadb:10.6.1
```

### 启动

```
docker run -p 3306:3306  --name mariadb  -e MARIADB_ROOT_PASSWORD=123456 --restart=always  -d mariadb:10.6.1
```

挂载：

- 配置文件的目录：/etc/mysql



更多的参考:

https://registry.hub.docker.com/_/mariadb?tab=description&page=1&ordering=last_updated





