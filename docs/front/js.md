# js整理
## 页面增加定时器
- 示例：五秒后发送验证码
```js
var time = 5;
var timer = setInterval(function(){
    if(time===0){
        //清除定时器
        clearInterval(timer);
        console.log("开始发送验证码")
    } else {
        time--;
    }
},1000);
```

## .attr()与.data()的区别
1.attr属性是必须写在html标签上，它属于dom属性，而data是储存于jquery对象模型上，它属于jquery对象属性，因此，它俩本质不一样；

2.attr的运行机制是：$.attr()取值和赋值都是找到html标签，直接操作该标签的属性;

3.data的运行机制是：页面第一次解析时，会将dom节点的attribute值存放到内存中， $.data()取值和赋值都是直接操作这个内存值，而不是dom元素；

所以：

1.避免attr和data的混合使用，以免因为数据不一样，产生bug;

2.attr赋值就用attr取值；

3.data赋值就用data取值
