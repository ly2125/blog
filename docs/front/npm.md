## 安装指定版本的nodejs和npm
 今天在deep上用`apt-get`命令安装nodejs的是否发现版本太低，node和npm的版本不对应。我们可以官网查看
 对应的版本:
[node和npm的版本对应地址](https://nodejs.org/zh-cn/download/releases/)
 ![](npm/images/npm1.jpg)

### 利用n安装



node有一个模块n，是专门用来管理node.js的版本的。

1、安装n模块：

```js
npm install -g n
```

2、升级node.js到最新稳定版

　　n stable

```bash
root@luyi-PC:/usr/local/bin# n stable
  installing : node-v14.15.4
       mkdir : /usr/local/n/versions/node/14.15.4
       fetch : https://nodejs.org/dist/v14.15.4/node-v14.15.4-linux-x64.tar.xz
   installed : v14.15.4 (with npm 6.14.10)
```

3、安装指定版本

```bash
n 14.15.4
```

查看对应的版本就发现OK了



```bash
root@luyi-PC:/usr/local/n/versions/node# npm -v
6.14.10
root@luyi-PC:/usr/local/n/versions/node# node -v
v14.15.4
```

