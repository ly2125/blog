
### docsify(文档)
如果你做了一个东西，需要写文档，可以选择使用他。 <br>
[官网文档](https://docsify.js.org/#/zh-cn)

一个神奇的文档网站生成器。docsify 是一个动态生成文档网站的工具。
不同于 GitBook、Hexo 的地方是它不会将 .md 转成 .html 文件，所有转换工作都是在运行时进行。这将非常实用，
如果只是需要快速的搭建一个小型的文档网站，或者不想因为生成的一堆 .html 文件“污染” commit 记录，
只需要创建一个 index.html 就可以开始写文档而且直接部署在GitHub Pages。

**个人使用反馈**

1. 我感觉还是比较上手，就是需要掌握md的语法；
2. 组件很多，具有一定的灵活性，文档写了我们可以部署到码云或者gitHup上,当然也很容易部署到自己服务器上；
3. 支持写html语法，还能支持vue,js等，使用感觉：五星好评！

### dbeaver（数据库连接工具）
开源免费的数据库连接工具，Navicat收费，在liunx系统上感觉破译比较麻烦，就用这个了
地址：[https://github.com/dbeaver/dbeaver/releases](https://github.com/dbeaver/dbeaver/releases)

### Another-Redis-Desktop-Manager(redis客户端）
一个redis可视化的连接工具，Redis-Desktop-Manager的代替品
地址：[https://github.com/qishibo/AnotherRedisDesktopManager/releases](https://github.com/qishibo/AnotherRedisDesktopManager/releases)


### zookeeper 客户端
   
- PrettyZoo :[PrettyZoo下载](https://github.com/vran-dev/PrettyZoo/releases)
- ZooInspector:[ZooInspector下载](https://issues.apache.org/jira/secure/attachment/12436620/ZooInspector.zip)
- zookeeper 插件，在idea中搜索zookeeper