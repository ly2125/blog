### CyC2018

码云上关注java最高，不仅讲解的java知识，世上还是牛人多。
[https://github.com/CyC2018](https://github.com/CyC2018/CS-Notes)

### javaguide（ java面试指导）

覆盖的知识非常广，作者也分享了很多的有用的东西。[https://snailclimb.gitee.io/javaguide/#/](https://snailclimb.gitee.io/javaguide/#/)

### 面试扫盲（分布式专题）

[https://github.com/doocs/advanced-java](https://github.com/doocs/advanced-java)

### java 8

很不错的一个东西，基础必备
[lingcoder.gitee.io/onjava8](http://lingcoder.gitee.io/onjava8)

### 多线程

-

深入线程多线程:  [https://redspider.gitbook.io/concurrent/di-san-pian-jdk-gong-ju-pian](https://redspider.gitbook.io/concurrent/di-san-pian-jdk-gong-ju-pian),githup地址：[https://github.com/RedSpider1/concurrent](https://github.com/RedSpider1/concurrent)

### 51

http://51gjie.com/

### 电子书下载

    https://github.com/itdevbooks/pdf
https://github.com/justjavac/free-programming-books-zh_CN
### 廖雪峰的官网网站

https://www.liaoxuefeng.com/

### Java-Interview-Advanced

https://gitee.com/shishan100/Java-Interview-Advanced

### 设计模式

https://github.com/iluwatar/java-design-patterns

### 