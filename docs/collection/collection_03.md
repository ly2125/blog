###  Mybaits(Plus)

- mybaits官网：[https://mp.baomidou.com/guide/](https://mp.baomidou.com/guide/)

### 阿里巴巴druid

[https://github.com/alibaba/druid/wiki/%E9%A6%96%E9%A1%B5](https://github.com/alibaba/druid/wiki/%E9%A6%96%E9%A1%B5)

###  redission

[地址点我](https://github.com/redisson/redisson/wiki/%E7%9B%AE%E5%BD%95)

### sentinel文档

https://github.com/alibaba/Sentinel/wiki/%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8

### Tendisplus

Tendis存储版是腾讯互娱CROS DBA团队 & 腾讯云数据库团队 自主设计和研发的开源分布式高性能KV存储。Tendis存储版完全兼容redis协议，并使用rocksdb作为存储引擎。用户可以通过redis client访问Tendis存储版，几乎不用修改代码。同时，Tendis存储版支持远超内存的磁盘容量，可以大大降低用户的存储成本。

类似于Redis Cluster, Tendis存储版使用去中心化的集群管理架构。数据节点之间通过gossip协议通讯，用户访问集群中的任意数据节，请求都能路由到正确的节点。并且集群节点支持自动发现、故障探测、自动故障切换、数据搬迁等能力，极大降低运维成本。

[地址点我](

### 系统监控

    https://cubic.jiagoujishu.com/docs/

### hutool文档
https://www.hutool.cn/