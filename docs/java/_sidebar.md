

* java基础 
  * [面试](java/java.md)
  * [字符串](java/java基础-字符串.md)
  * [异常](java/java基础-异常.md)
  * [命名规则](java/java基础-命名规则.md)
  * [位运算](java/java基础-位运算.md)
  * [反射](java/java基础-反射.md)
  * [final](java/java基础-final.md)
* JVM
  * [内存区域划分](java/JVM内存区域划分.md)
  
* 架构篇
  * [参数校验](java/架构-参数校验.md)
  
* spring-cloud
  * [eureka](/java/cloud/eureka.md)
  * [zookeeper](java/cloud/zookeeper.md)
  * [ribbon](java/cloud/rabbion.md)
  * [openfeign](java/cloud/openfeign.md)
  * [hystrix](java/cloud/hystrix.md)
  * [nacos](java/cloud/nacos.md)
  * [sentinel](java/cloud/sentinel.md)
  


 








  


  





