# Math
public class MathDemo {    public static void main(String args[]){

/**

* abs求绝对值

*/

System.out.println(Math.abs(-10.4));    //10.4
System.out.println(Math.abs(10.1));     //10.1
/**
* ceil天花板的意思，就是返回大的值，注意一些特殊值
*/
System.out.println(Math.ceil(-10.1));   //-10.0
System.out.println(Math.ceil(10.7));    //11.0
System.out.println(Math.ceil(-0.7));    //-0.0
System.out.println(Math.ceil(0.0));     //0.0
System.out.println(Math.ceil(-0.0));    //-0.0
/**
* floor地板的意思，就是返回小的值
*/
System.out.println(Math.floor(-10.1));  //-11.0
System.out.println(Math.floor(10.7));   //10.0
System.out.println(Math.floor(-0.7));   //-1.0
System.out.println(Math.floor(0.0));    //0.0
System.out.println(Math.floor(-0.0));   //-0.0
/**
* max 两个中返回大的值,min和它相反，就不举例了
*/
System.out.println(Math.max(-10.1, -10));   //-10.0
System.out.println(Math.max(10.7, 10));     //10.7
System.out.println(Math.max(0.0, -0.0));    //0.0
/**
* random 取得一个大于或者等于0.0小于不等于1.0的随机数
*/
System.out.println(Math.random());  //0.08417657924317234
System.out.println(Math.random());  //0.43527904004403717
/**
* rint 四舍五入，返回double值
* 注意.5的时候会取偶数
*/
System.out.println(Math.rint(10.1));    //10.0
System.out.println(Math.rint(10.7));    //11.0
System.out.println(Math.rint(11.5));    //12.0
System.out.println(Math.rint(10.5));    //10.0
System.out.println(Math.rint(10.51));   //11.0
System.out.println(Math.rint(-10.5));   //-10.0
System.out.println(Math.rint(-11.5));   //-12.0
System.out.println(Math.rint(-10.51));  //-11.0
System.out.println(Math.rint(-10.6));   //-11.0
System.out.println(Math.rint(-10.2));   //-10.0
/**
* round 四舍五入，float时返回int值，double时返回long值
*/
System.out.println(Math.round(10.1));   //10
System.out.println(Math.round(10.7));   //11
System.out.println(Math.round(10.5));   //11
System.out.println(Math.round(10.51));  //11
System.out.println(Math.round(-10.5));  //-10
System.out.println(Math.round(-10.51)); //-11
System.out.println(Math.round(-10.6));  //-11
System.out.println(Math.round(-10.2));  //-10
}
}
常用值与函数：

Math.PI 记录的圆周率
Math.E 记录e的常量
Math中还有一些类似的常量，都是一些工程数学常用量。

Math.abs 求绝对值
Math.sin 正弦函数 Math.asin 反正弦函数
Math.cos 余弦函数 Math.acos 反余弦函数
Math.tan 正切函数 Math.atan 反正切函数 Math.atan2 商的反正切函数
Math.toDegrees 弧度转化为角度 Math.toRadians 角度转化为弧度
Math.ceil 得到不小于某数的最大整数
Math.floor 得到不大于某数的最大整数
Math.IEEEremainder 求余
Math.max 求两数中最大
Math.min 求两数中最小
Math.sqrt 求开方
Math.pow 求某数的任意次方, 抛出ArithmeticException处理溢出异常
Math.exp 求e的任意次方
Math.log10 以10为底的对数
Math.log 自然对数
Math.rint 求距离某数最近的整数（可能比某数大，也可能比它小）
Math.round 同上，返回int型或者long型（上一个函数返回double型）
Math.random 返回0，1之间的一个随机数

用法实例：
double s=Math.sqrt(7);
double x=Math.pow(2,3) //计算2的3次方


## 接口和类对比

| 类目 | 方法 | 成员变量|构造方法|
|-----|----|--|--|
|抽象类|无限制|无限制|子类通过调用构造方法链调用构造方法，抽象类不能用new操作符实例化
|  接口|public abstract |public abstract|无构造方法，不能用new实例化。


抽象类：

1，抽象类不能用new来创造他的实例（在子类的构造方法中实例化），但是可以作为一种数据类型。下边是正确的（假设A是抽象类）：

A[] a=new a[10];

2,抽象类中可以没有抽象方法，但是有抽象方法该类一定是抽象类。

3，子类可以覆盖父类的抽象方法，并将它定义为abstract（但是很少用，一般在都实现了这种方法，但是他在当父亲的方法实现在子类变的无效时是很有用的，此时必须用他）。

4，即使子类的父亲是具体的，子类也可以是抽象的（如A与Object）；

5，抽象类中的方法是非静态的，构造方法定义为抽象protected（因为他只能被子类使用）；

compare	变量	构造方法	方法
抽象类	无限制	子类通过调用构造方法链调用构造方法，抽象类不能用new操作符实例化	无限制
接口	所有的变量为public static final	无构造方法，不能用new实例化。	public abstract
一个类可以继承多个接口（implements），但是只能继承一个类，接口可以继承接口，但是接口不能继承类；

接口：

1，接口是对功能的拓展，它的目的是指明相关或者不相关类的多个对象的共同行为。

2，接口中只有常量和抽象方法。// 接口中只有的方法全是abstract。

3，接口可以继承接口（用extends）。


## 数组(Array)和列表(ArrayList)有什么区别？什么时候应该使用Array而不是ArrayList？
参考答案
下面列出了Array和ArrayList的不同点：
Array可以包含基本类型和对象类型，ArrayList只能包含对象类型。
Array大小是固定的，ArrayList的大小是动态变化的。
ArrayList提供了更多的方法和特性，比如：addAll()，removeAll()，iterator()等等。
对于基本类型数据，集合使用自动装箱来减少编码工作量。但是，当处理固定大小的基本数据类型的时候，这种方式相对比较慢。