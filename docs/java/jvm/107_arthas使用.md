`Arthas` 是Alibaba开源的Java诊断工具，深受开发者喜爱。在线排查问题，无需重启；动态跟踪Java代码；实时监控JVM状态。

`Arthas` 支持JDK 6+，支持Linux/Mac/Windows，采用命令行交互模式，同时提供丰富的 `Tab` 自动补全功能，进一步方便进行问题的定位和诊断。

- Github: https://github.com/alibaba/arthas
- 文档: https://arthas.aliyun.com/doc/

- 文档地址2：https://arthas.aliyun.com/doc/arthas-tutorials.html?language=cn

## 程序准备&下载&启动

### 程序准备（本地是jdk8的环境）

我们准备下边的一段代码，模仿我们需要调试的程序：

```
/**
 * @author luyi
 * 阿里Arthas测试
 */
public class ArthasTest {
    private static HashSet hashSet = new HashSet();

    public static void main(String[] args) {
        // 模拟 CPU 过高
        cpuHigh();
        // 模拟线程死锁
        deadThread();
        // 不断的向 hashSet 集合增加数据
        addHashSetThread();
    }

    /**
     * 19 * 不断的向 hashSet 集合添加数据
     * 20
     */
    public static void addHashSetThread() {
        // 初始化常量
        new Thread(() -> {
            int count = 0;
            while (true) {
                try {
                    hashSet.add("count" + count);
                    Thread.sleep(1000);
                    count++;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void cpuHigh() {
        new Thread(() -> {
            while (true) {

            }
        }).start();
    }

    /**
     * 死锁
     */
    private static void deadThread() {
        /** 创建资源 */
        Object resourceA = new Object();
        Object resourceB = new Object();
        // 创建线程
        Thread threadA = new Thread(() -> {
            synchronized (resourceA) {
                System.out.println(Thread.currentThread() + " get ResourceA");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread() + "waiting get resourceB");
                synchronized (resourceB) {
                    System.out.println(Thread.currentThread() + " get resourceB");
                }
            }
        });

        Thread threadB = new Thread(() -> {
            synchronized (resourceB) {
                System.out.println(Thread.currentThread() + " get ResourceB");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread() + "waiting get resourceA");
                synchronized (resourceA) {
                    System.out.println(Thread.currentThread() + " get resourceA");
                }
            }
        });
        threadA.start();
        threadB.start();
    }
}
```

### 下载

- github下载arthas

  wget https://alibaba.github.io/arthas/arthas‐boot.jar

- Gitee 下载
   wget https://arthas.gitee.io/arthas‐boot.jar

- liunx 下载

  curl -O https://arthas.aliyun.com/arthas-boot.jar

### 启动

下载完成后，我们直接用`java -jar arthas-boot.jar` 启动即可，我们发现如下：

* ```bash
  luyi@luyi-PC:/data/home/luyi/dev-soft$ /home/luyi/开发软件/jdk1.8.0_161/bin/java -jar arthas-boot.jar --telnet-port 9998 --http-port -1
  [INFO] arthas-boot version: 3.5.1
  [INFO] Found existing java process, please choose one and input the serial number of the process, eg : 1. Then hit ENTER.
  
  * [1]: 28341 com.lz.test.jvm.ArthasTest
    [2]: 3784 com.intellij.idea.Main
    [3]: 28317 org.jetbrains.jps.cmdline.Launcher
  ```
  
  发现第一个就是我们需要调试的程序，我们输入1，回车即可。

#### 指定jdk
  但是我们会发现进入我们的程序调试的时候，却报错：

```
WARN] Current VM java version: 1.8 do not match target VM java version: 11, attach may fail.
[WARN] Target VM JAVA_HOME is /data/home/luyi/dev-soft/ideaIU-2021.1.1/idea-IU-211.7142.45/jbr, arthas-boot JAVA_HOME is /data/home/luyi/开发软件/jdk1.8.0_161/jre, try to set the same JAVA_HOME.
[ERROR] Start arthas failed, exception stack trace: 
java.io.IOException: Non-numeric value found - int expected
        at sun.tools.attach.HotSpotVirtualMachine.readInt(HotSpotVirtualMachine.java:299)
        at sun.tools.attach.HotSpotVirtualMachine.loadAgentLibrary(HotSpotVirtualMachine.java:63)
        at sun.tools.attach.HotSpotVirtualMachine.loadAgentLibrary(HotSpotVirtualMachine.java:79)
        at sun.tools.attach.HotSpotVirtualMachine.loadAgent(HotSpotVirtualMachine.java:103)
        at com.taobao.arthas.core.Arthas.attachAgent(Arthas.java:118)
        at com.taobao.arthas.core.Arthas.<init>(Arthas.java:26)
        at com.taobao.arthas.core.Arthas.main(Arthas.java:137)
```

  arthas是jdk11启动的，这个时候我们就要指定jdk启动：

```
${java_home}/java -jar  arthas-boot.jar
```

示例：/home/luyi/开发软件/jdk1.8.0_161/bin/java -jar arthas-boot.jar  

进入到下边页面，说明成功：

![image-20210609231145567](images/arthas启动.png)

  

##  常用命令

### help

通过这个命令，我们就知道arthas有哪些命令，以及这个命令的大概作用。

### dashboard

![dashboard](images/arthas_dashboard.png)

tips:如果我们需要退出dashaoard,按ctrl+c即可

### thread

#### 线程堆栈

在dashboard，我们发现thread-0这个线程占用的cpu非常高，我们需要找到这个线程占用cpu高的代码,我们就可以用：`thread thread-id`

```bash
[arthas@28341]$ thread 13
"Thread-0" Id=13 RUNNABLE
    at com.lz.test.jvm.ArthasTest.lambda$cpuHigh$1(ArthasTest.java:43)
    at com.lz.test.jvm.ArthasTest$$Lambda$1/897913732.run(Unknown Source)
    at java.lang.Thread.run(Thread.java:748)
```

这样我们就能定位是ArthasTest的cpuHigh()方法占用的cpu高，并指定了行数为43行。

####  线程死锁

输入 thread -b 可以查看线程死锁

```bash
[arthas@28341]$ thread -b
"Thread-2" Id=15 BLOCKED on java.lang.Object@11a7e5c8 owned by "Thread-1" Id=14
    at com.lz.test.jvm.ArthasTest.lambda$deadThread$3(ArthasTest.java:82)
    -  blocked on java.lang.Object@11a7e5c8
    -  locked java.lang.Object@2d73ba2f <---- but blocks 1 other threads!
    at com.lz.test.jvm.ArthasTest$$Lambda$3/226170135.run(Unknown Source)
    at java.lang.Thread.run(Thread.java:748)
```



#### jad(反编译代码)

通过可以命令，我们可以反编译代码（比如一个功能线上的期望值与我们预想的不符，怀疑运维没有部署成功，就可以用这个命令去证实我们的猜测）

```
[arthas@28341]$ jad com.lz.test.jvm.ArthasTest

ClassLoader:                                                                                                                                             
+-sun.misc.Launcher$AppClassLoader@18b4aac2                                                                                                              
  +-sun.misc.Launcher$ExtClassLoader@24fcd133                                                                                                            

Location:                                                                                                                                                
/home/luyi/workspace/product/test/zzz-test-ly/jvm/target/classes/                                                                                        

       /*
        * Decompiled with CFR.
        */
       package com.lz.test.jvm;
       
       import java.util.HashSet;
       
       public class ArthasTest {
           private static HashSet hashSet = new HashSet();
       
           public static void main(String[] args) {

/*14*/         ArthasTest.cpuHigh();
/*16*/         ArthasTest.deadThread();
/*18*/         ArthasTest.addHashSetThread();
           }
```

