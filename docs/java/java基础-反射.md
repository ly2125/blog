## 反射的概述

JAVA反射机制是在运行状态中，对于任意一个类，都能够知道这个类的所有属性和方法；对于任意一个对象，都能够调用它的任意一个方法和属性；这种动态获取的信息以及动态调用对象的方法的功能称为java语言的反射机制。

要想解剖一个类,必须先要获取到该类的字节码文件对象。而解剖使用的就是Class类中的方法.所以先要获取到每一个字节码文件对应的Class类型的对象.



## 获取Class的三种方式

**反射使用的条件就是：必须先得到代表的字节码的Class，Class类用于表示.class文件（字节码）。**

首先我们需要知道，java代码在计算机中会经历三个阶段：

1. Source源代码阶段：.java被编译成*.class字节码文件。
2. Class类对象阶段：.class字节码文件被类加载器加载进内存，并将其封装成Class对象（用于在内存中描述字节码文件），Class对象将原字节码文件中的成员变量抽取出来封装成数组Field[],将原字节码文件中的构造函数抽取出来封装成数组Construction[]，将成员方法封装成数组Method[]。当然Class类内不止这三个，还封装了很多，我们常用的就这三个。
3. RunTime运行时阶段：使用new创建对象的过程。



![在这里插入图片描述](https://luyi-docs.oss-cn-chengdu.aliyuncs.com/blog/20210207162006.png)



在三个阶段中，我们都可以获取对应的Class,然后进行反射操作，以下就是java获取Class的三种方式：

1. 【Source源代码阶段】 Class.forName(“全类名”)：将字节码文件加载进内存，返回Class对象；
   多用于配置文件，将类名定义在配置文件中，通过读取配置文件加载类。
2. 【Class类对象阶段】 类名.class：通过类名的属性class获取；
   多用于参数的传递
3. 【Runtime运行时阶段】对象.getClass()：此方法是定义在Objec类中的方法，因此所有的类都会继承此方法，多用于对象获取字节码的方式。



## 反射API操作(待完成)

https://blog.csdn.net/Mr_wxc/article/details/105812627

## 使用反射注意到的问题

#### 性能问题

1.使用反射基本上是一种解释操作，用于字段和方法接入时要远慢于直接代码。因此Java反射机制主要应用在对灵活性和扩展性要求很高的系统框架上,普通程序不建议使用。

2.反射包括了一些动态类型，所以JVM无法对这些代码进行优化。因此，反射操作的效率要比那些非反射操作低得多。我们应该避免在经常被 执行的代码或对性能要求很高的程序中使用反射。

#### **使用反射会模糊程序内部逻辑**

程序人员希望在源代码中看到程序的逻辑，反射等绕过了源代码的技术，因而会带来维护问题。反射代码比相应的直接代码更复杂。

#### 安全限制

使用反射技术要求程序必须在一个没有安全限制的环境中运行。如果一个程序必须在有安全限制的环境中运行，如Applet，那么这就是个问题了

#### 内部暴露

由于反射允许代码执行一些在正常情况下不被允许的操作（比如访问私有的属性和方法），所以使用反射可能会导致意料之外的副作用－－代码有功能上的错误，降低可移植性。反射代码破坏了抽象性，因此当平台发生改变的时候，代码的行为就有可能也随着变化。

 

### Java反射可以访问和修改私有成员变量，那封装成private还有意义么？

既然小偷可以访问和搬走私有成员家具，那封装成防盗门还有意义么？这是一样的道理，并且Java从应用层给我们提供了安全管理机制——安全管理器，每个Java应用都可以拥有自己的安全管理器，它会在运行阶段检查需要保护的资源的访问权限及其它规定的操作权限，保护系统免受恶意操作攻击，以达到系统的安全策略。所以其实反射在使用时，内部有安全控制，如果安全设置禁止了这些，那么反射机制就无法访问私有成员。

 

### 反射是否真的会让你的程序性能降低?

1.反射大概比直接调用慢50~100倍，但是需要你在执行100万遍的时候才会有所感觉

2.判断一个函数的性能，你需要把这个函数执行100万遍甚至1000万遍

3.如果你只是偶尔调用一下反射，请忘记反射带来的性能影响

4.如果你需要大量调用反射，请考虑缓存。

5.你的编程的思想才是限制你程序性能的最主要的因素

