- **下载地址**：https://github.com/alibaba/nacos/releases
- **官方文档**：https://nacos.io/zh-cn/docs/what-is-nacos.html
    - **配置文档**：https://nacos.io/zh-cn/docs/system-configurations.html







##  注册中心使用



###  添加依赖

```java
<dependency>
    <groupId>com.alibaba.boot</groupId>
    <artifactId>nacos-discovery-spring-boot-starter</artifactId>
    <version>${latest.version}</version>
</dependency>
```

或者

```java
  <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
            <version>${nacos.version}</version>
  </dependency>
```

​          建议使用后者，方便依赖统一管理

### 配置文件

建议配置到`bootstrap`文件中，原因：[点我](/java/springBoot/bootstrap.md)

```java
#nacos注册中心配置
spring.cloud.nacos.discovery.server-addr=localhost:8848
#组,不配置默认为default_group
spring.cloud.nacos.discovery.group=baseGroup
#空间，不配置默认为public
spring.cloud.nacos.discovery.namespace=c67ed1e6-2ea8-42fb-961e-4ccaac5ae861
```

注意：其中的group 和 namespace 都是自己在服务端页面自己配置的,更多配置见  最上边的配置文件

### 代码使用

####  开启注册中心@EnableDiscoveryClient

在springBoot启动类添加 @EnableDiscoveryClient 注解(加了才能注册)，然后nacos的界面：服务管理-服务列表就会看到启动的服务

####  @NacosInjected

使用 @NacosInjected 注入 Nacos 的 NamingService 实例：

```java
@Controller
@RequestMapping("discovery")
public class DiscoveryController {
    @NacosInjected
    private NamingService namingService;
    @RequestMapping(value = "/get", method = GET)
    @ResponseBody
    public List<Instance> get(@RequestParam String serviceName) throws NacosException {
        return namingService.getAllInstances(serviceName);
    }
}

@SpringBootApplication
public class NacosDiscoveryApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosDiscoveryApplication.class, args);
    }
}
```

### Nacos Discovery Actuator Endpoint

Nacos Discovery 内部提供了一个 Endpoint, 对应的 endpoint id 为 nacosdiscovery，其 Actuator Web Endpoint URI 为/actuator/nacos-discovery

## 配置中心的使用

### 添加依赖

```java
<dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
            <version>${nacos.version}</version>
        </dependency>
```

或者

```java
<dependency>
    <groupId>com.alibaba.boot</groupId>
    <artifactId>nacos-config-spring-boot-starter</artifactId>
    <version>${nacos.version}</version>
</dependency>
```

### 配置

```java
##配置中心地址
spring.cloud.nacos.config.server-addr=localhost:8848
## 文件拓展名
spring.cloud.nacos.config.file-extension=yml
#命名空间，不配置默认为public
spring.cloud.nacos.config.namespace=c67ed1e6-2ea8-42fb-961e-4ccaac5ae861
#分组，不配置默认为default_group
spring.cloud.nacos.config.group=baseGroup
```

在 Nacos Spring Cloud 中，dataId 的完整格式如下：

```java
${prefix}-${spring.profile.active}.${file-extension}
```

prefix 默认为 spring.application.name 的值，也可以通过配置项 spring.cloud.nacos.config.prefix来配置。
spring.profile.active 即为当前环境对应的 profile，详情可以参考 Spring Boot文档。 注意：当 spring.profile.active 为空时，对应的连接符 - 也将不存在，dataId 的拼接格式变成 ${prefix}.${file-extension}
file-exetension 为配置内容的数据格式，可以通过配置项 spring.cloud.nacos.config.file-extension 来配置。目前只支持 properties 和 yaml 类型

####  配置的优先级

Spring Cloud Alibaba Nacos Config 目前提供了三种配置能力从 Nacos 拉取相关的配置。

- A: 通过 spring.cloud.nacos.config.shared-configs[n].data-id 支持多个共享 Data Id 的配置

- B: 通过 spring.cloud.nacos.config.extension-configs[n].data-id 的方式支持多个扩展 Data Id 的配置

- C: 通过内部相关规则(应用名、应用名+ Profile )自动生成相关的 Data Id 配置

  

  注意：当三种方式共同使用时，他们的一个优先级关系是：A < B < C

#### 更多高级配置

| 配置项           | Key                                      | 默认值         | 说明                                                         |
| ---------------- | ---------------------------------------- | -------------- | ------------------------------------------------------------ |
| 服务端地址       | spring.cloud.nacos.config.ser ver-addr   |                | Nacos Server 启动监听的 ip 地址和端口                        |
| 配置对应的DataId | spring.cloud.nacos.config.na me          |                | 先取 prefix，再取name，最后取 spr ing.application.na me      |
| 配置对应的DataId | spring.cloud.nacos.config.pre fix        |                | 先取 prefix，再取name，最后取 spr ing.application.na me      |
| 配置内容编码     | spring.cloud.nacos.config.enc ode        |                | 读取的配置内容对应的编码                                     |
| GROUP            | spring.cloud.nacos.config.gro up         | DEFAULT_G ROUP | 配置对应的组                                                 |
| 文件扩展名       | spring.cloud.nacos.config.file Extension | properties     | 配置项对应的文件扩展名，目前支持properties 和 ya ml(yml)     |
| 获取配置超时时间 | spring.cloud.nacos.config.tim eout       | 3000           | 客户端获取配置的超时时间(毫秒)                               |
| 接入点           | spring.cloud.nacos.config.end point      |                | 地域的某个服务的入口域名，通过此域名可以动态地拿到服务端地址 |

| 配置项                          | Key                                           | 默认值 | 说明                                                         |
| ------------------------------- | --------------------------------------------- | ------ | ------------------------------------------------------------ |
| 命名空间                        | spring.cloud.nacos.config.na mespace          |        | 常用场景之一是不同环境的配置的区分隔离，例如开发测试环境和生产环境的资源（如配置、服务）隔离等 |
| AccessKey                       | spring.cloud.nacos.config.acc essKey          |        | 当要上阿里云时，阿里云上面的一个云账号名                     |
| SecretKey                       | spring.cloud.nacos.config.sec retKey          |        | 当要上阿里云时，阿里云上面的一个云账号密码                   |
| Nacos Server 对应的context path | spring.cloud.nacos.config.con textPath        |        | Nacos Server 对外暴露的 context path                         |
| 集群                            | spring.cloud.nacos.config.clu sterName        |        | 配置成 Nacos 集群名称（比如阿里的服务器部署在杭州和广州，这时候起两个集群名称，尽量让同一个机房的微服务调用，以提升性能） |
| 共享配置                        | spring.cloud.nacos.config.sha redDataids      |        | 共享配置的 DataI d, "," 分割                                 |
| 共 享配 置动态刷新              | spring.cloud.nacos.config.refr eshableDataids |        | 共享配置中需要动态刷新的 DataId,"," 分割                     |
| 自定义Data Id配置               | spring.cloud.nacos.config.ext Config          |        | 属性是个集合，内部由 ConfigPOJO组成。Config 有 3个属性，分别是 dat aId, group 以及 refresh |

### 实现Bean 动态刷新

在springCloud中可以通过 @RefreshScope这个注解来实现配置自动刷新

#### @value属性动态刷新

```java

    @RestController@RequestMapping("/config")
    @RefreshScope
    public class ConfigController {
        @Value("${useLocalCache:false}")
        private boolean useLocalCache; 
        @RequestMapping("/get") 
        public boolean get() { 
            return useLocalCache; 
        }}
	}
```

#### @ConfigurationProperties Bean属性动态刷新

第一步：定义配置类

```
**
 * @author luyi
 */
@ConfigurationProperties(prefix = "user")
@Data
public class UserConfig {
    private String username = "test";
}
```

第二步：

在启动类上加上@EnableConfigurationProperties

```
@EnableConfigurationProperties(UserConfig.class)
```

第三步：定义测试类

```
@RestController
@RequestMapping("/userConfig")
@RefreshScope
public class UserConfigController {
    @Autowired
    private UserConfig userConfig;

    @RequestMapping("/get")
    public UserConfig get(){
        return userConfig;
    }
}
```

我们会发现首先username是3，当我们在nacos中修改添加user.username后，就会发生改变

Nacos Confg 支持标准 Spring Cloud `@RefreshScope` 特性，即应用订阅某个Nacos 配置后，当配置内容变化时，Refresh Scope Beans 中的绑定配置的属性将有条件的更新。所谓的条件是指 Bean 必须:

- 必须条件：Bean 的声明类必须标注 @RefreshScope
- 二选一条件： 属性（非 static 字段）标注 `@Value`或者 `@ConfigurationProperties Bean`

### 配置持久化

1. 安装数据库，版本要求：5.6.5+
2. 初始化mysql数据库，数据库初始化文件：nacos-mysql.sql
3. 修改conf/application.properties文件，增加支持mysql数据源配置（目前只支持mysql），添加mysql数据源的url、用户名和密码。

> spring.datasource.platform=mysql db.num=1
> db.url.0=jdbc:mysql://127.0.0.1:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
> db.user=root db.password=root

###  Nacos Config Actuator Endpoint

Na c os Conf i g 内部提供了一个 End poi nt , 对应的 En dpoi n t ID 为 nacos-config，其 Actuator Web Endpoint URI 为 /actuator/nacosconfig。

其中，Endpoint 暴露的 json 中包含了三种属性:

- NacosConfigProperties: 当前应用 Nacos 的基础配置信息。

- RefreshHistory: 配置刷新的历史记录。

- Sources: 当前应用配置的数据信息。

 

由于 Aliyun Java Initializr 所生成的应用工程默认激活 Spring Boot Actuator

Endpoints（JMX 和 Web），具体配置存放在 application.properties 文件中，同时， Actuator Web 端口设置为 8081，内容如下：

```
management.endpoints.jmx.exposure.include=*
management.endpoints.web.exposure.include=*
management.endpoint.health.show-details=always
```



 

# nacos单机与集群

###    nacos下载

- 首先到githup上下载tar的安装包，下载地址：[https://github.com/alibaba/nacos/tags](https://github.com/alibaba/nacos/tags)

- 然后解压：`tar -zxvf nacos-server-$version.tar.gz`

注意：我们下载的版本要和我们项目中引入的nacos对应，在nacos注册中心和配置中心的依赖中，我们看到有个client依赖，我们下载的版本对之对应即可：

```
<dependency>
  <groupId>com.alibaba.nacos</groupId>
  <artifactId>nacos-client</artifactId>
  <version>xxx</version>
  <scope>compile</scope>
</dependency>
```

### 单机启动

- 启动命令(standalone代表着单机模式运行，非集群模式):  `sh startup.sh -m standalone`，不加参数默认集群启动。

**`注意：`**   如果您使用的是ubuntu系统，或者运行脚本报错提示[[符号找不到，可尝试如下运行：  bash startup.sh -m standalone

- 启动后，输入在浏览器中输入：http://ip:8848/nacos/（默认的端口是8848，可以到conf的application.properties属性文件中修改），如果能够看到页面就说明启动成功，登陆的账号和密码默认都是nacos

如果需要关闭nacos，到nacos的bin目录： `sh shutdown.sh`


`注意`：Nacos在不同系统中运行的模式是不一样的，在liunx中默认集群模式，windows默认单机模式



### 配置集群

官方文档：[https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html](https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html)

![nacos集群图](/images/nacos集群图.jpg)

#### 配置cluster.conf

在nacos的解压目录nacos/的conf目录下，有配置文件cluster.conf，请每行配置成ip:port。（请配置3个或3个以上节点）

> 200.8.9.16:8848
> 200.8.9.17:8848
> 200.8.9.18:8848

#### 确定数据源

### 使用内置数据源

无需进行任何配置，使用内置数据源的时候如下启动：

```
sh startup.sh -p embedded
```

### 使用外置数据源

生产使用建议至少主备模式，或者采用高可用数据库（初始化 ：`nacos-mysql.sql`，这个sql在安装包conf目录下中有），启动的命令如下

```
sh startup.sh
```



## 4.4 客户端配置

多个url用逗号连接就可。