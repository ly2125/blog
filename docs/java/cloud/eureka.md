## 简介

Spring-Cloud Euraka是Spring
Cloud集合中一个组件，它是对Euraka的集成，用于服务注册和发现。Eureka是Netflix中的一个开源框架。但是在18年的时候已经停止开源更新（依然可以使用），githup地址：https://github.com/Netflix/eureka/wiki。

## springCloud项目整合

### 服务端

#### 引入依赖

```
<!----版本根据spring-cloud的版本-->
 <dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
 </dependency>
```
####  添加@EnableEurekaServer注解

在启动类上添加`@EnableEurekaServer`注解，标识自己是eureka注册中心服务端：

```
@SpringBootApplication(exclude = {GsonAutoConfiguration.class})
@EnableEurekaServer
public class EurekaStart9001 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaStart9001.class, args);
    }
}
```

#### 配置yml

```
spring:
  application:
    name: eureka-service
server:
  port: 9001
  tomcat:
    threads:
      max: 50

eureka:
  instance:
    hostname: eureke9001.com  #eureka实例
  client:
    # false表示不向注册中心注册自己
    register-with-eureka: false
    ## false 表示自己是注册中心， 不需要去检索服务，我的职责是维护服务实例
    fetch-registry: false
```

注意：我在host文件中配置了eureke9001.com指向127.0.0.1

#### 验证

 在浏览器输入localhost:9001,进入eureka页面就说明服务端成功

![image-20210515235134417](/java/cloud/images/image-20210515235134417.png)

### 客户端

#### 引入依赖

```
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```

添加@EnableEurekaClient注解

```
@SpringBootApplication(exclude = {GsonAutoConfiguration.class})
@EnableEurekaClient
public class EurekaClientOrderStart9004 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaClientOrderStart9004.class, args);
    }
}
```

#### 配置yml

```
spring:
  application:
    name: eureka-client-order
server:
  port: 9004
  tomcat:
    threads:
      max: 50

eureka:
  client:
    #  表示将自己注册到注册中心，默认为true
    register-with-eureka: true
    ## 从注册中心拉取已有的注册信息，默认 true（集群的时候必须为true,才能使用ribbon负载 均衡）
    fetch-registry: true
    service-url:
      ## eureka交互的地址
      defaultZone: http://eureka9001.com:9001/eureka
```

#### 验证

进入注册中心，在服务列表中找到这个服务实例就说明注册成功。



##  集群配置

###  服务端



如果配置一个服务节点，出现了单点故障，就会导致整个服务不可用。

eureka服务端的集群就是相互注册，配置如下：

- 9001节点配置

```
eureka:
  instance:
    hostname: eureke9001.com  #eureka实例
  client:
    # false表示不向注册中心注册自己
    register-with-eureka: false
    ## false 表示自己是注册中心， 不需要去检索服务，我的职责是维护服务实例
    fetch-registry: false
    service-url:
      ## eureka交互的地址
      defaultZone: http://eureka9000.com:9000/eureka
```

- 9000节点配置

```
eureka:
  instance:
    hostname: eureka9000.com  #eureka实例
  client:
    # false表示不向注册中心注册自己
    register-with-eureka: false
    ## false 表示自己是注册中心， 不需要去检索服务，我的职责是维护服务实例
    fetch-registry: false
    service-url:
      ## eureka交互的地址
      defaultZone: http://eureka9001.com:9001/eureka
```

我们看到defaultZone中，他们彼此相互注册

### 客户端

​     在defaultZone中配置所有的服务节点，逗号分开即可：

```
eureka:
  client:
    #  表示将自己注册到注册中心，默认为true
    register-with-eureka: true
    ## 从注册中心拉取已有的注册信息，默认 true（集群的时候必须为true,才能使用ribbon负载 均衡）
    fetch-registry: true
    service-url:
      ## eureka交互的地址
      defaultZone: http://eureka9000.com:9000/eureka,http://eureka9001.com:9001/eureka
```

验证：

分别访问localhost:9000和localhost:90001,以下满足则说明配置成功：

- DS Replicas 中 是其他服务节点的信息（相互注册）
- 都能看到注册的客户端节点



## 自我保护机制

保护模式主要用于一组客户端和Eureka Server之间存在网络分区场景下的保护。一旦进入保护模式，

Eureka Server将会尝试保护其服务注册表中的信息，不再删除服务注册表中的数据，也就是不会注销任何微服务。

如果在Eureka Server的首页看到以下这段提示，则说明Eureka进入了保护模式：

> EMERGENCY! EUREKA MAY BE INCORRECTLY CLAIMING INSTANCES ARE UP WHEN THEY'RE NOT. 
>
> RENEWALS ARE LESSER THAN THRESHOLD AND HENCE THE INSTANCES ARE NOT BEING EXPIRED JUST TO BE SAFE 



###  为什么会产生Eureka自我保护机制？*



为了防止EurekaClient可以正常运行，但是 与 EurekaServer网络不通情况下，EurekaServer不会立刻将EurekaClient服务剔除

 

###  什么是自我保护模式？

默认情况下，如果EurekaServer在一定时间内没有接收到某个微服务实例的心跳，EurekaServer将会注销该实例（默认90秒）。但是当网络分区故障发生(延时、卡顿、拥挤)时，微服务与EurekaServer之间无法正常通信，以上行为可能变得非常危险了——因为微服务本身其实是健康的，此时本不应该注销这个微服务。Eureka通过“自我保护模式”来解决这个问题——当EurekaServer节点在短时间内丢失过多客户端时（可能发生了网络分区故障），那么这个节点就会进入自我保护模式。 

在自我保护模式中，Eureka Server会保护服务注册表中的信息，不再注销任何服务实例。

它的设计哲学就是宁可保留错误的服务注册信息，也不盲目注销任何可能健康的服务实例。一句话讲解：好死不如赖活着

 

综上，自我保护模式是一种应对网络异常的安全保护措施。它的架构哲学是宁可同时保留所有微服务（健康的微服务和不健康的微服务都会保留）也不盲目注销任何健康的微服务。使用自我保护模式，可以让Eureka集群更加的健壮、稳定。



### 如何关闭自我保护机制

#### 服务端

```
  eureka
    server:
      #关闭自我保护机制，保证不可用服务被及时踢除
      enable-self-preservation: false
      eviction-interval-timer-in-ms: 2000
```

在服务端配置如上，并在控制台看到如下，则说明成功：



> **THE SELF PRESERVATION MODE IS TURNED OFF. THIS MAY NOT PROTECT INSTANCE EXPIRY IN CASE OF NETWORK/OTHER PROBLEMS.**

#### 客户端

```yml
eureka:
#心跳检测与续约时间
#开发时设置小些，保证服务关闭后注册中心能即使剔除服务
  instance:
  #Eureka客户端向服务端发送心跳的时间间隔，单位为秒(默认是30秒)
    lease-renewal-interval-in-seconds: 1
  #Eureka服务端在收到最后一次心跳后等待时间上限，单位为秒(默认是90秒)，超时将剔除服务
    lease-expiration-duration-in-seconds: 2
```

心跳检测时间和续约时间根据实际情况配置



