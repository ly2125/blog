

## 设置超时时间
ribbon在本地负载均衡着很重要的作用，但是`ribbon的默认请求超时时间是1秒`,很容易超时，这个时候我们就要根据实际情况修改超时时间：

```yaml
ribbon:
  ReadTimeout: 5000
  ConnectTimeout: 5000
```