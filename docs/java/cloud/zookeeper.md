

##  服务发现

###  springBoot整合



- 第一步：引入依赖

  ```
  <dependencies>
      <dependency>
          <groupId>org.springframework.cloud</groupId>
          <artifactId>spring-cloud-starter-zookeeper-discovery</artifactId>
          <!--排除自带的低版本zookeeper-->
          <exclusions>
              <exclusion>
                  <groupId>org.apache.zookeeper</groupId>
                  <artifactId>zookeeper</artifactId>
              </exclusion>
          </exclusions>
      </dependency>
      <dependency>
          <groupId>org.apache.zookeeper</groupId>
          <artifactId>zookeeper</artifactId>
          <version>${zookeeper.version}</version>
      </dependency>
  </dependencies>
  ```

​      spring-cloud自带的`zookeeper`的版本比较低，我们可以移除后引入合适的版本

- 第二步：配置启动类

  在启动类上加上`@EnableDiscoveryClient`注解，标识自己是服务发现客户端

- 第三步：配置yml

  ```
  spring:
    application:
      name: zk-client-pay
    cloud:
      zookeeper:
        connect-string: localhost:2181
  ```
- 第四步：验证是否注册成功
    - 首先服务没有进错
    - 在zk能够找到这个节点（可以通过命令和客户端的方式）

###  注册的服务节点是临时节点还是持久节点？

  进入zkCli命令, 通过`ls /services/zk-client-pay`,我们停止服务会发现节点不存在了，重启后就会发现节点id发生了变化，就此说明是`临时节点`

  