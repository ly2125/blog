##  简介

​		Zipkin是一个分布式跟踪系统。它有助于收集解决服务体系结构中的延迟问题所需的时序数据。功能包括该数据的收集和查找。如果您在日志文件中有跟踪ID，则可以直接跳至该跟踪ID。否则，您可以基于诸如服务，操作名称，标签和持续时间之类的属性进行查询。将为您汇总一些有趣的数据，例如服务中花费的时间百分比以及操作是否失败。

​		Zipkin UI还提供了一个依赖关系图，该关系图显示了每个应用程序中跟踪了多少请求。这对于识别包括错误路径或对不赞成使用的服务的调用在内的汇总行为可能会有所帮助。

​      

## 文档地址

​      官方文档：https://zipkin.io/

​	 githup地址：https://github.com/openzipkin/openzipkin.github.io

​		

## 下载

官网文档说了这两个下载最新jar包的方法：

https://search.maven.org/remote_content?g=io.zipkin&a=zipkin-server&v=LATEST&c=exec

或者

```
curl -sSL https://zipkin.io/quickstart.sh | bash -s
```



## springCloud整合

- 第一步：引入依赖

  注意：在服务的生产者和消费者都需要引入

  ```xml
  <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-zipkin</artifactId>
  </dependency>
  ```

- 配置yml

  ```
  spring:
    zipkin:
      base-url: http://localhost:9411/
    sleuth:
      sampler:
        # 采集率值介于0-1之间，1则表示全部采集,一般0.5就可以
        probability: 1
  ```

- 启动zipkin

  java -jar zipkin-server-2.23.2-exec.jar



​	这个时候，我们进入http://localhost:9411/，就能看到对应的信息

